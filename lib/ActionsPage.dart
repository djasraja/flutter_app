
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/DataBaseSqlite/DBHelper.dart';
import 'package:flutter_shopping_app/Model/LoginModel.dart';
import 'package:flutter_shopping_app/MyListUI.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'BottomNavigationTab.dart';
import 'FireBaseService/fireBaseServices.dart';
import 'MyAccount.dart';
import 'SignIn.dart';
import 'SignUp.dart';
import 'main.dart';
import 'main.dart';

List<popUpItems> choicesForLogin = [];




class ActionsPage extends StatefulWidget {
  @override
  _ActionsPageState createState() => _ActionsPageState();
}

class _ActionsPageState extends State<ActionsPage> {
  bool loginstatus=false;

  Future loginFunc() async{
    List<LoginModel> loginStatus=await DBHelper().getLoginConfirmation();
    print("${loginStatus.length}++++++++++++++++++++++++++");
    //print("${loginStatus[loginStatus.length - 1].status}++++++++++++++++++++++++++");
    if(loginStatus.length>0){
      if(loginStatus[loginStatus.length - 1].status ){
        print("${loginStatus[loginStatus.length - 1].status}+++++++++++INSIDE+++++++++++++++");
        setState(() {
          print("${loginStatus[loginStatus.length - 1].status}+++++++++++INSIDE-2+++++++++++++++");
          loginstatus=true;
        });
        return loginstatus;
      }

    }
    return loginstatus;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //loginFunc();
    //Future.value(loginFunc());
    loginFunc().then((value) {
      setState(() {
        if(value){
          setState(() {
            choicesForLogin= [
              popUpItems(values: "MY ACCOUNT", icon: MdiIcons.accountCircleOutline),
              popUpItems(values: "MY ORDERS", icon: MdiIcons.packageVariant),
              popUpItems(values: "MY LIST", icon: MdiIcons.fileDocumentOutline),
              popUpItems(values: "SIGN OUT", icon: MdiIcons.logoutVariant),];
          });
        }else{
          setState(() {
            choicesForLogin=[popUpItems(values: "REGISTER", icon: MdiIcons.accountCircleOutline),
              popUpItems(values: "SIGN IN", icon: MdiIcons.login),];
          });

        }
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
    Navigator.pop(context);
    },
        child: Scaffold(
          appBar: AppBar(
//          leading: IconButton(
//            icon: Icon(Icons.arrow_back_ios),
//            onPressed: (){
//              Navigator.pop(context);
//            },
//          ),
            title: Text(
              "Zionx",
              style: TextStyle(color: Colors.lightGreen),
            ),
            //backgroundColor: Colors.white,
          ),
          bottomNavigationBar: BottomNavigationTab(currentIndex: 3,),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                for(popUpItems choice in choicesForLogin) Container(
                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.grey,))),
                  child: ListTile(
                    leading: Icon(choice.icon,color: Colors.green,size: 30,),
                    title: Text(choice.values,style: TextStyle(fontSize: 16),),
                    onTap: ()async{
                      if(choice.values == "REGISTER") {Navigator.push(context, MaterialPageRoute(builder: (_) => SignUP(),),);}
                      else if (choice.values == "SIGN IN") {Navigator.push(context, MaterialPageRoute(builder: (_) => SignIn(),),);}
                      else if (choice.values == "MY ACCOUNT") {Navigator.push(context, MaterialPageRoute(builder: (_) => MyAccount(),),);}
                      else if (choice.values == "MY ORDERS") {}
                      else if (choice.values == "MY LIST") {Navigator.push(context, MaterialPageRoute(builder: (_) => MyListUI(),),);}
                      else if (choice.values == "SIGN OUT") {AuthServic().signOut();await DBHelper().deleteFromLogin();await DBHelper().deleteFromUserDetails();setState((){loginstatus=false;});Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => HomePage(),),);}
                    },
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
