import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/BottomNavigationTab.dart';
import 'package:flutter_shopping_app/theme.dart';

import 'main.dart';

class AllCategoriesPage extends StatefulWidget {
  bool BottomBar;
  @override
  _AllCategoriesPageState createState() => _AllCategoriesPageState();

  AllCategoriesPage({this.BottomBar=true});
}

class _AllCategoriesPageState extends State<AllCategoriesPage> {
  List<dynamic> CategoryItemDetails=[["assets/1.jpeg","Grocery"],["assets/2.jpeg","Dairy"],["assets/3.jpeg","Packaged Food"],["assets/4.jpeg","Fruits"],["assets/5.jpeg","Appliances"],["assets/1.jpeg","Grocery"],["assets/2.jpeg","Dairy"],["assets/3.jpeg","Packaged Food"],["assets/4.jpeg","Fruits"],["assets/5.jpeg","Appliances"],["assets/6.jpeg","Appliances"]];

  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage()));
        },
        child: Scaffold(
          appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
            title: Text(
              "Shop By Category",
              style: TextStyle(color: Colors.lightGreen),
            ),
            actions: <Widget>[
              Container(
                child: PopupMenuButton<popUpItems>(
                  //onSelected: (){},
                  tooltip: "Actions",
                  itemBuilder: (BuildContext context) {
                    return choicesBeforeLogin.map((popUpItems choice) {
                      return PopupMenuItem<popUpItems>(
                        value: choice,
                        child: Container(
                          //padding: EdgeInsets.all(4),
                          child: Row(
                            children: <Widget>[
                              Icon(choice.icon),
                              SizedBox(
                                width: 10,
                              ),
                              Text(choice.values)
                            ],
                          ),
                        ),
                      );
                    }).toList();
                  },
                ),
              )
            ],
            //backgroundColor: Colors.white,
          ),
          bottomNavigationBar: widget.BottomBar == true ? BottomNavigationTab(currentIndex: 1,) : null,
          body: GridView.count(
            crossAxisSpacing: 1,
            shrinkWrap: true,
            crossAxisCount: 2,
            children: <Widget>[
              for(int i=0;i<CategoryItemDetails.length;i++) Center(
                child: Container(height:MediaQuery.of(context).size.width/2,width:MediaQuery.of(context).size.width/2,child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(CategoryItemDetails[i][0],fit: BoxFit.fill,height: MediaQuery.of(context).size.width/3 ,width:MediaQuery.of(context).size.width/3),
                    SizedBox(height:3),
                    Text(CategoryItemDetails[i][1],style: TextStyle(fontSize: 16),),
                  ],
                ),decoration: BoxDecoration(border: Border.all(width: 1,color: Colors.grey)),),
              )
            ],
          ),
        ),
      ),
    );
  }
}
