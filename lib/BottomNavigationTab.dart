import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/CartPage.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:badges/badges.dart';
import 'ActionsPage.dart';
import 'AllCategoriesPage.dart';
import 'MyAccount.dart';
import 'main.dart';

class BottomNavigationTab extends StatefulWidget {
  int currentIndex=0;
  BottomNavigationTab({this.currentIndex});
  @override
  _BottomNavigationTabState createState() => _BottomNavigationTabState();
}

class _BottomNavigationTabState extends State<BottomNavigationTab> {


  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: widget.currentIndex == null ?0 : widget.currentIndex,
      selectedItemColor: Colors.green,
      unselectedItemColor: Colors.green,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      iconSize: 30,
      items: [
        BottomNavigationBarItem(
          icon: Icon(MdiIcons.homeOutline,),
          title: Text("Home"),
        ),
        BottomNavigationBarItem(
          icon: Icon(MdiIcons.viewGridOutline,),
          title: Text("Category"),
        ),
        BottomNavigationBarItem(
          icon: Icon(MdiIcons.cartOutline,),
          title: Text("Cart"),
        ),
//        BottomNavigationBarItem(
//          title: Text("Cart"),
//          //icon: Icon(MdiIcons.cartOutline,),
//          icon: new Stack(
//            children: <Widget>[
//              new Icon(MdiIcons.cartOutline),
//              new Positioned(
//                right: 0,
//                child: new Container(
//                  padding: EdgeInsets.all(1),
//                  decoration: new BoxDecoration(
//                    color: Colors.green,
//                    borderRadius: BorderRadius.circular(6),
//                  ),
//                  constraints: BoxConstraints(
//                    minWidth: 20,
//                    minHeight: 20,
//                  ),
//                  child: new Text(
//                    '33',
//                    style: new TextStyle(
//                      color: Colors.white,
//                      fontSize: 14,
//                    ),
//                    textAlign: TextAlign.center,
//                  ),
//                ),
//              )
//            ],
//          ),
//        ),
        BottomNavigationBarItem(
          icon: Icon(MdiIcons.dotsVertical,),
          title: Text("Actions"),
        ),
      ],
      onTap: (index){
        setState(() {
          widget.currentIndex=index;
        });

        switch(index){
          case 0:{Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage()));}
          break;
          case 1:{Navigator.of(context).push(MaterialPageRoute(builder: (context) => AllCategoriesPage()));}
          break;
          case 2:{Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartPage()));}
          break;
          case 3:{Navigator.of(context).push(MaterialPageRoute(builder: (context) => ActionsPage()));}
          break;
        }
      },
    );
  }
}
