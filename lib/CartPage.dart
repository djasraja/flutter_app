import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/Model/CartModel.dart';
import 'package:flutter_shopping_app/PaymentAddressPage.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:http/http.dart' as http;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'BottomNavigationTab.dart';
import 'DataBaseSqlite/DBHelper.dart';
import 'http_services.dart';

Future<List<CartModel>> getCartItemsFromDB() async {
  print("ggteetttttttttttttttt CART");
  var dbHelper = DBHelper();
  Future<List<CartModel>> cartItem = dbHelper.getCartItems();
  return cartItem;
}

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  List cartItems = [];
  List<CartModel> cartItemFromDb =[];
  double TotalPrice=0.0,TotalSaving=0.0;

  Future getCartItems() async {
    Map<String, dynamic> dataItems;
    cartItems.clear();
    cartItemFromDb.clear();
    cartItemFromDb = await getCartItemsFromDB();
    //print("ggtee2222222222222222222222222=${listFromDb.length} ${listFromDb[0].listId}");
    for (int i = 0; i < cartItemFromDb.length; i++) {
      //print(getProductByIdUrl + "${cartItemFromDb[i].cartId}");
      print(cartItemFromDb[i].cartId);
      http.Response res =
          //await http.get(postProductPriceTagsUrl + "${cartItemFromDb[i].cartId}");
      await http.post(postProductDetailsByPriceID,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          "priceId": "${cartItemFromDb[i].priceTagID}",
          "array" : "true"
        }),
      );
      if (res.statusCode == 200) {
        print("STATUS 200");
        dataItems = jsonDecode(res.body);
        setState(() {
          cartItems.add(dataItems);
        });
      }
    }
    print("${cartItemFromDb.length} +++++++++++++++++++++++++++++++++");
    print(cartItems.length);
    print(cartItems);
  }


  void getTotalAmountAndSaving() async{
    await getCartItems();
//    cartItemFromDb.clear();
//    cartItemFromDb = await getCartItemsFromDB();
    TotalPrice=0.0;
    TotalSaving=0.0;
    print("${cartItemFromDb.length} ---------------------${TotalPrice}--------${TotalSaving}--");
    if(cartItemFromDb.length == 0){
      setState(() {
        TotalPrice=0.0;
        TotalSaving=0.0;
      });
    }
    for(int i=0;i<cartItemFromDb.length;i++){
      TotalPrice = TotalPrice + (int.tryParse(cartItems[i]['price']) * cartItemFromDb[i].qty);
      TotalSaving = TotalSaving + ((int.tryParse(cartItems[i]['mrp'])-int.tryParse(cartItems[i]['price'])) * cartItemFromDb[i].qty);
    }
  }

//  void removeCartItem(int prod_id) async{
//    getCartItems();
//    //cartItemFromDb.clear();
//    int qty=0;
//    //cartItemFromDb = await getCartItemsFromDB();
//    for(int i=0;i<cartItemFromDb.length;i++){
//      if(cartItemFromDb[i].cartId == prod_id){
//        qty=cartItemFromDb[i].qty;
//        break;
//      }
//    }
//    for(int i=0;i<cartItems.length;i++){
//      if(cartItems[i]['id'] == prod_id){
//        TotalPrice = TotalPrice - (cartItems[i]['sales_price'] * qty);
//        TotalSaving = TotalSaving - ((cartItems[i]['price']-cartItems[i]['sales_price']) * qty);
//      }
//    }
//
//  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    getCartItems();
    getTotalAmountAndSaving();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              "VIEW CART",
              style: TextStyle(color: Colors.lightGreen),
            ),
//            leading: IconButton(
//              icon: Icon(Icons.arrow_back_ios),
//              onPressed: () {
//                Navigator.pop(context);
//              },
//            ),
          ),
          bottomNavigationBar: BottomNavigationTab(
            currentIndex: 2,
          ),
          body: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white70,
                  border: Border(
                      bottom: BorderSide(
                          width: 2, color: Colors.black54)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("${cartItems.length} Item(s) in cart",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),
                        SizedBox(height: 5,),
                        Text("TOTAL SAVINGS \u{20B9}${TotalSaving}",style: TextStyle(fontSize: 14),),
                      ],
                    ),
                    Text("TOTAL PRICE \u{20B9}${TotalPrice}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Colors.green),),
                  ],
                ),
              ),
          (cartItems.isNotEmpty && cartItemFromDb.isNotEmpty)
          ?Expanded(
        child: new ListView.builder(
          shrinkWrap: true,
          //scrollDirection: Axis.horizontal,
          //physics: NeverScrollableScrollPhysics(),
          itemCount: (cartItems == null && cartItemFromDb == null)  ? false : cartItems.length,
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        width: 2, color: Colors.green)),
              ),
              margin: EdgeInsets.all(5),
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.network(
                        cartItems[index]['img'],
                        fit: BoxFit.fill,
                        height: 90,
                        width: MediaQuery.of(context).size.width *
                            0.20,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          CartModel model=CartModel();
                          model.cartId=cartItems[index]['id'];
                          var dbHelper=DBHelper();
                          dbHelper.deleteFromCart(model);
                          _showToast(context);
                          setState(() {
                            print("getting total ammount.........................");
                            getTotalAmountAndSaving();
                          });
                        },
                        child: Text(
                          "REMOVE",
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.redAccent,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    width:
                    MediaQuery.of(context).size.width * 0.70,
                    //color: Colors.red,
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            padding: EdgeInsets.all(5),
                            //color: Colors.blue,
                            //height: 30,
                            child: Text(
                              "${cartItems[index]['title']} : ${cartItems[index]['qty']} ${cartItems[index]['unit_title']}",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            //color: Colors.green,
                            height: 60,
                            child: Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "MRP : \u{20B9}${cartItems[index]['mrp']}",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black87,
                                    decoration: TextDecoration
                                        .lineThrough,
                                  ),
                                ),
                                Text(
                                  "ZPrice : \u{20B9}${cartItems[index]['price']}",
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontSize: 16),
                                ),
                                Text(
                                  "Save : \u{20B9}${int.tryParse(cartItems[index]['mrp']) - int.tryParse(cartItems[index]['price'])}",
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontSize: 13),
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          //padding: EdgeInsets.all(5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.center,
                                width: 40,
                                height: 30,
                                decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60),topLeft: Radius.circular(60),)),
                                child: RaisedButton(
                                  onPressed: () async{
                                    print("Minus");
                                    CartModel model=CartModel();
                                    var dbHelper=DBHelper();
                                    model.id=cartItemFromDb[index].id;
                                    model.cartId=cartItemFromDb[index].cartId;
                                    model.priceTagID=cartItemFromDb[index].priceTagID;
                                    if(cartItemFromDb[index].qty == 1){
                                      setState(() {TotalPrice=0;
                                      TotalSaving=0;});
                                      dbHelper.deleteFromCart(model);
                                    }else{
                                      setState(() {TotalPrice=0;
                                      TotalSaving=0;});
                                      model.qty = cartItemFromDb[index].qty - 1;
                                      dbHelper.updateCart(model);
                                    }
                                    setState(() {});
                                    await getTotalAmountAndSaving();
                                  },
                                  color: Colors.white,
                                  textColor: Colors.green,
                                  child: Center(child: Text("-",style: TextStyle(fontSize: 20),)),
                                ),
                              ),
                              SizedBox(width: 5,),
                              Container(child: Text("${cartItemFromDb[index].qty}",style: TextStyle(fontSize: 20),)),
                              SizedBox(width: 5,),
                              Container(
                                alignment: Alignment.center,
                                width: 40,
                                height: 30,
                                decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomRight: Radius.circular(60),topRight: Radius.circular(60),)),
                                child: RaisedButton(
                                  onPressed: () async {
                                    print("Plus");
                                    if((cartItemFromDb[index].qty + 1) <= cartItems[index]['stock']){
                                      setState(() {TotalPrice=0;
                                      TotalSaving=0;});
                                      CartModel model=CartModel();
                                      var dbHelper=DBHelper();
                                      model.id=cartItemFromDb[index].id;
                                      model.cartId=cartItemFromDb[index].cartId;
                                      model.cartId=cartItemFromDb[index].priceTagID;
                                      model.qty = cartItemFromDb[index].qty + 1;
                                      dbHelper.updateCart(model);
                                      setState(() {});
                                      await getTotalAmountAndSaving();
                                    }else{
                                      Scaffold.of(context).showSnackBar(
                                        SnackBar(
                                          content: const Text('Reached maximum limit of product'),
                                        ),
                                      );
                                    }
                                  },
                                  color: Colors.white,
                                  textColor: Colors.green,
                                  child: Center(child: Text("+",style: TextStyle(fontSize: 20),)),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            );
          },
        ),
      )
          : Container(
        height: MediaQuery.of(context).size.height*0.5,
        child:
        Center( child: //CircularProgressIndicator(),
        Text(
          "The Cart is empty :(",
          style: TextStyle(fontSize: 20),
          textAlign: TextAlign.center,
        )
        ),
      ),
              (cartItems.isNotEmpty && cartItemFromDb.isNotEmpty && (TotalPrice > 0))?RaisedButton(
                onPressed: (){print("-----------------${TotalPrice}--------------");
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PaymentAddressPage(totalAmount: TotalPrice,totalSaving: TotalPrice,)));
                },
                color: Colors.green,
                textColor: Colors.white,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text('PROCEED TO CHECKOUT',textAlign: TextAlign.center,),
                ),
              ):new SizedBox()
            ],
          ),
        ),
      ),
    );
  }

  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Deleted from List'),
      ),
    );
  }

}
