import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/DataBaseSqlite/DBHelper.dart';
import 'package:flutter_shopping_app/Model/UserDetailsModel.dart';
import 'package:flutter_shopping_app/http_services.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:http/http.dart' as http;

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final passwordController = new TextEditingController();
  final newPasswordController = new TextEditingController();
  bool _obscuredText = true;

  Future<String> _getUsername()async{
    List<UserDetailsModel> userInfo=await DBHelper().getUserDetails();
    if(userInfo.length > 0)
      return userInfo[0].username;
    else
      return null;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    passwordController.dispose();
    newPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: basicTheme(),
        home: WillPopScope(
          onWillPop: () {
            Navigator.pop(context);
          },
          child: Scaffold(
            // resizeToAvoidBottomInset: false,
            // resizeToAvoidBottomPadding: false,
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
              title: Text(
                "CHANGE PASSWORD",
                style: TextStyle(color: Colors.lightGreen),
              ),
              //backgroundColor: Colors.white,
            ),
            body: FutureBuilder(
              future: _getUsername(),
              builder: (context, snapshot) {
                if(snapshot.hasData){
                  print(snapshot.data);
                }
                return SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(top: 20, left: 10, right: 10),
                    child: snapshot.hasData?Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        //SizedBox(height: 10,),
                        Text("ENTER OLD PASSWORD",),
                        SizedBox(height: 5,),
                        TextField(
                          controller: passwordController,
                          obscureText: true,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            counterText: "",
                            //isDense: true,
                            contentPadding: EdgeInsets.all(8),
                            hintText: "Password",
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.green),
                            ),
                          ),
                        ),
                        SizedBox(height: 10,),
                        Text("ENTER NEW PASSWORD",),
                        SizedBox(height: 5,),
                        TextField(
                          controller: newPasswordController,
                          obscureText: _obscuredText,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            counterText: "",
                            isDense: true,
                            contentPadding: EdgeInsets.all(8),
                            hintText: "Password",
                            suffixIcon: IconButton(
                              iconSize: 20,
                              color: Colors.lightGreen,
                              icon: Icon(_obscuredText
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                              onPressed: () {
                                setState(() {
                                  _obscuredText = !_obscuredText;
                                });
                              },
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.green),
                            ),
                          ),
                        ),
                        SizedBox(height: 10,),
                        SizedBox(
                          height: 50,
                          width: double.infinity,
                          child: RaisedButton(
                            color: Colors.green,
                            child: Text(
                              "SAVE CHANGES",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            onPressed: () async{
                              if(snapshot.data == null){
                                showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context){
                                    return AlertDialog(
                                      title: Text("Error!"),
                                      content: Text("Something Went Wrong! Please try again later."),
                                      contentPadding: EdgeInsets.all(10),
                                      actions: <Widget>[
                                        FlatButton(
                                          child: Text("OK"),
                                          onPressed: (){
                                            Navigator.of(context).pop();
                                          },
                                        )
                                      ],
                                    );
                                  },
                                );
                              }else{
                                bool flag=await _changePassWord(snapshot.data);
                                if(flag){
                                  passwordController.clear();
                                  newPasswordController.clear();
                                  Scaffold.of(context).showSnackBar(
                                    SnackBar(
                                      content: const Text("Password Successfully Updated!"),
                                    )
                                  );
                                }else{
                                  showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (BuildContext context){
                                      return AlertDialog(
                                        title: Text("Alert"),
                                        content: Text("Your Old Password is incorrect!"),
                                        contentPadding: EdgeInsets.all(10),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text("OK"),
                                            onPressed: (){
                                              Navigator.of(context).pop();
                                            },
                                          )
                                        ],
                                      );
                                    },
                                  );
                                }
                              }
                            },
                          ),
                        ),
                      ],
                    ): Center(child: CircularProgressIndicator(),),
                  ),
                );
              }
            ),
          ),
        )
    );
  }

  Future<bool> _changePassWord(String username)async{
    print(postChangePasswordUrl);
    print("${passwordController.text} ${newPasswordController.text}");
    http.Response res=await http.post(postChangePasswordUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "username" : "9999999999",
        "password" : "${passwordController.text}",
        "newPassword" : "${newPasswordController.text}"
      }),);
    print("REsponse : ${res.statusCode}");
    if(res.statusCode == 200){
      Map<String, dynamic> list=jsonDecode(res.body) as Map<String,dynamic>;
      print(list);
      if(list['status']==true){
        return true;
      }else
        return false;
    }else
      return false;
  }

}
