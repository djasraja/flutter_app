import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/Model/UserDetailsModel.dart';
import 'package:flutter_shopping_app/http_services.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:http/http.dart' as http;
import 'DataBaseSqlite/DBHelper.dart';
import 'Model/CartModel.dart';


Future<List<CartModel>> getCartItemsFromDB() async {
  print("ggteetttttttttttttttt CART");
  var dbHelper = DBHelper();
  Future<List<CartModel>> cartItem = dbHelper.getCartItems();
  return cartItem;
}

Future<List> _getProfileDetails()async{
  List MyProfileDetails=[];
  MyProfileDetails=await DBHelper().getUserDetails();
  print("Future : ${MyProfileDetails}");
  return MyProfileDetails;
}

class ConfirmAndOrderDetailsPage extends StatefulWidget {
  double totalAmount,totalSaving;
  int AddressID,TimeSlot=4;
  @override
  _ConfirmAndOrderDetailsPageState createState() => _ConfirmAndOrderDetailsPageState();

  ConfirmAndOrderDetailsPage(
      {@required this.totalAmount, @required this.totalSaving, @required this.AddressID, @required this.TimeSlot});
}

class _ConfirmAndOrderDetailsPageState extends State<ConfirmAndOrderDetailsPage> {

  List<CartModel> cartItemFromDb =[];
  List<Map<String,dynamic>> productArray=[];
  List<UserDetailsModel> userInfo=[];

  Future createCartProductArray(){
    Map<String,dynamic> prod={};
    for(int i=0;i<cartItemFromDb.length;i++){
      prod['id']=cartItemFromDb[i].cartId;
      prod['priceId']=cartItemFromDb[i].priceTagID;
      prod['inCart']=cartItemFromDb[i].qty;
      productArray.add(prod);
    }
  }

  Future<bool> placeOrder()async{
    //cartItemFromDb.isEmpty?null:cartItemFromDb.clear();
    //userInfo.isNotEmpty?userInfo.clear():null;
    //productArray.isNotEmpty?productArray.clear():null;
    await getCartItemsFromDB().then((value) async {
      cartItemFromDb=value;
      await createCartProductArray();
    } );
    //productArray = await createCartProductArray();
    userInfo = await _getProfileDetails();
    print("${userInfo[0].id} ${cartItemFromDb} ${productArray} ");
    print("@@@@@@@@@@@@@@@@@@@@@@@@@@ ${productArray}");
    http.Response res =  await http.post(postOrderUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "userId" : userInfo[0].id,
        "assigneeId" : "0",
        "products" : productArray,
        "address" : widget.AddressID,
        "time_slot" : "4"
      }),
    );
    if (res.statusCode == 200) {
      print("STATUS 200");
     if(jsonDecode(res.body)['status']==true){
       print("Order Placed Successfully");
       return true;
     }else{
       print(jsonDecode(res.body)['message']);
       return false;
     }
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: basicTheme(),
    home: WillPopScope(
      onWillPop: () {
      Navigator.pop(context);
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          title: Text(
            "CONFIRM ORDER",
            style: TextStyle(color: Colors.lightGreen),
          ),
          //backgroundColor: Colors.white,
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                RaisedButton(
                  onPressed: ()async{await placeOrder().then((bool value) {
                    if(value)
                      print("Hurraayyyyyyyyyyyyyyy");
                    else
                      print("SOOOOOOOOOOOOOOOOO SAADDDDDDDDDDDDD");
                  });},
                  color: Colors.green,
                  child: Text("CONFIRM ORDER",style: TextStyle(color: Colors.white),),
                ),
              ],
            ),
          ),
        ),
      ),
    )
    );
  }
}
