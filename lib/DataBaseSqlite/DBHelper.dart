
import 'dart:async';
import 'dart:io' as io;
import 'package:flutter_shopping_app/Model/CartModel.dart';
import 'package:flutter_shopping_app/Model/MyListModel.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter_shopping_app/Model/LoginModel.dart';
import 'package:flutter_shopping_app/Model/UserDetailsModel.dart';

class DBHelper{
  static Database db_instance;
  final String List_Table_name="MyList";
  final String Cart_Table_name="Cart";
  final String Login_Table_name="login";
  final String UserDetail_Table_name="userDetails";

  Future<Database> get db async{
    if(db_instance == null)
      db_instance = await initDB();
    else
      print("Not creating");
    return db_instance;
  }

  initDB() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path,"zionx.db");
    print(path);
    var db = await openDatabase(path,version: 1,onCreate: onCreateFunc);
    return db;
  }

  void onCreateFunc(Database db, int version) async {
    print("Created REQUIRED TABLES");
    await db.execute("CREATE TABLE $List_Table_name (id INTEGER PRIMARY KEY AUTOINCREMENT,ListId INTEGER,priceTagID INTEGER);");
    await db.execute("CREATE TABLE $Cart_Table_name (id INTEGER PRIMARY KEY AUTOINCREMENT,cartId INTEGER,priceTagID INTEGER,qty INTEGER);");
    await db.execute("CREATE TABLE $Login_Table_name (id INTEGER PRIMARY KEY AUTOINCREMENT,message TEXT,status BOOLEAN,token TEXT);");
    await db.execute("CREATE TABLE $UserDetail_Table_name (id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT,firstName TEXT,lastName TEXT,phone INTEGER,altrPhone INTEGER,email TEXT,userId INTEGER,password TEXT);");
  }

  /*
  CRUD FUNCTIONS
  */

  Future<List<UserDetailsModel>> getUserDetails() async{
    var db_connection= await db;
    List<Map> userDetailsFromDb = await db_connection.rawQuery('SELECT * FROM $UserDetail_Table_name');
    print(userDetailsFromDb);
    List<UserDetailsModel> userInfo = new List();
    for(int i=0;i<userDetailsFromDb.length;i++){
      UserDetailsModel userObj = new UserDetailsModel();
      userObj.username=userDetailsFromDb[i]['username'];
      userObj.firstName=userDetailsFromDb[i]['firstName'];
      userObj.lastName=userDetailsFromDb[i]['lastName'];
      userObj.phone=userDetailsFromDb[i]['phone'];
      userObj.altrPhone=userDetailsFromDb[i]['altrPhone']=="null"?0:userDetailsFromDb[i]['altrPhone'];
      userObj.email=userDetailsFromDb[i]['email'];
      userObj.id=userDetailsFromDb[i]['userId'];
      userObj.password=userDetailsFromDb[i]['password'];
      userInfo.add(userObj);
    }
    return userInfo;
  }

  void addToUserDetails(UserDetailsModel userInfo)async{
    var db_connection= await db;
    String query = 'INSERT INTO $UserDetail_Table_name(username,firstName,lastName,phone,altrPhone,email,userId,password) VALUES (\'${userInfo.username}\',\'${userInfo.firstName}\',\'${userInfo.lastName}\',\'${userInfo.phone}\',\'${userInfo.altrPhone}\',\'${userInfo.email}\',\'${userInfo.id}\',\'${userInfo.password}\')';
    await db_connection.transaction((txn) async {
      return await txn.rawInsert(query);
    });
  }

  void deleteFromUserDetails() async{
    var db_connection= await db;
    String query = 'DELETE FROM $UserDetail_Table_name';
    await db_connection.transaction((txn) async {
      return await txn.rawInsert(query);
    });
    print("........................Signed out...............................");
  }

  Future<List<LoginModel>> getLoginConfirmation() async{
    var db_connection= await db;
    List<Map> list = await db_connection.rawQuery('SELECT * FROM $Login_Table_name');
    print(list);
    List<LoginModel> login = new List();
    for(int i=0;i<list.length;i++){
      LoginModel loginobj = new LoginModel();
      loginobj.message= list[i]['message'];
      loginobj.status=list[i]['status'].toLowerCase() == 'true'?true:false;
      loginobj.token= list[i]['token'];
      login.add(loginobj);
    }
    return login;
  }
  void addToLogin(LoginModel login) async{
    var db_connection= await db;
    String query = 'INSERT INTO $Login_Table_name(message,status,token) VALUES (\'${login.message}\',\'${login.status}\',\'${login.token}\')';
    await db_connection.transaction((txn) async {
      return await txn.rawInsert(query);
    });
  }
  void deleteFromLogin() async{
    var db_connection= await db;
    String query = 'DELETE FROM $Login_Table_name';
    await db_connection.transaction((txn) async {
      return await txn.rawInsert(query);
    });
    print("........................Signed out...............................");
  }

  //Cart DB
  Future<List<CartModel>> getCartItems() async{
    var db_connection= await db;
    List<Map> list = await db_connection.rawQuery('SELECT * FROM $Cart_Table_name');
    print(list);
    List<CartModel> myCartItems = new List();
    for(int i=0;i<list.length;i++){
      CartModel cartobj = new CartModel();
      cartobj.cartId= list[i]['cartId'];
      cartobj.priceTagID=list[i]['priceTagID'];
      cartobj.qty=list[i]['qty'];
      cartobj.id= list[i]['id'];
      myCartItems.add(cartobj);
    }
    return myCartItems;
  }
  //Add new to Cart
  void addNewToCart(CartModel list) async{
    var db_connection= await db;
    String query = 'INSERT INTO $Cart_Table_name(cartId,priceTagID,qty) VALUES (\'${list.cartId}\',\'${list.priceTagID}\',\'${list.qty}\')';
    await db_connection.transaction((txn) async {
      return await txn.rawInsert(query);
    });
  }
  void updateCart(CartModel list) async{
    var db_connection= await db;
    print("updating........${list.qty}");
    String query = 'UPDATE $Cart_Table_name SET qty=\'${list.qty}\' WHERE id=\'${list.id}\'';
    await db_connection.transaction((txn) async {
      return await txn.rawQuery(query);
    });
  }
  //Delete from cart
  void deleteFromCart(CartModel list) async{
    var db_connection= await db;
    String query = 'DELETE FROM $Cart_Table_name WHERE priceTagID=${list.priceTagID}';
    await db_connection.transaction((txn) async {
      return await txn.rawInsert(query);
    });
  }

  //My List DB
  Future<List<MyListModel>> getList() async{
    var db_connection= await db;
    List<Map> list = await db_connection.rawQuery('SELECT * FROM $List_Table_name');
    print(list);
    List<MyListModel> myList = new List();
    for(int i=0;i<list.length;i++){
      MyListModel listobj = new MyListModel();
      listobj.listId= list[i]['ListId'];
      listobj.id= list[i]['id'];
      myList.add(listobj);
    }
    return myList;
  }
  //Add new to list
  void addNewToList(MyListModel list) async{
    var db_connection= await db;
    bool flag=false;
    List<MyListModel> inList=await getList();
    for(int i=0;i<inList.length;i++){
      if(inList[i].listId == list.listId){
        flag=true;
        break;
      }
    }
    String query = 'INSERT INTO $List_Table_name(listId) VALUES (\'${list.listId}\')';
    if(!flag){
      await db_connection.transaction((txn) async {
        return await txn.rawInsert(query);
      });
    }
  }
  //Delete from list
  void deleteFromList(MyListModel list) async{
    var db_connection= await db;
    String query = 'DELETE FROM $List_Table_name WHERE ListId=${list.listId}';
    await db_connection.transaction((txn) async {
      return await txn.rawInsert(query);
    });
  }


}