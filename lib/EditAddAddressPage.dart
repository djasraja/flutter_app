//import 'dart:html';

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_shopping_app/http_services.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:http/http.dart' as http;

class EditAddAddress extends StatefulWidget {
  String title;
  int userId;
  Map<String,dynamic> data;

  @override
  _EditAddAddressState createState() => _EditAddAddressState();

  EditAddAddress({@required this.title,this.data,@required this.userId});
}

class _EditAddAddressState extends State<EditAddAddress> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var Fname,Address,Landmark,Pincode,Area,City,State,MobileNo;
  int AddressId;


  Future<bool> addNewAddress()async{
    print("ADDING NEW ADDRESS.........");
    print(postAddNewAddressUrl);
    http.Response res =await http.post(postAddNewAddressUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "contactName" : "${Fname.text}",
        "contactNumber" : "${MobileNo.text}",
        "nearBy" : "${Landmark.text}",
        "pincode" : "${Pincode.text}",
        "city" : "${City.text}",
        "addressLine" : "${Address.text}",
        "userId" : "${widget.userId}"
      }),);
    print("REsponse : ${res.statusCode}");
    if (res.statusCode == 200){
      print("Address STATUS 200");
      print(res.body);
      Map<String,dynamic> addressResBody=await jsonDecode(res.body) as Map;
      if(addressResBody['status']==true){
        return true;
      }else{
        print("Response status false----------------");
        return false;
      }
    }else{
      print("Response status is not 200----------------");
      return false;
    }
  }

  Future<bool> updateAddress()async{
    print("UPDATING ADDRESS.........");
    print(postEditAddressUrl);
    http.Response res =await http.post(postEditAddressUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "contactName" : "${Fname.text}",
        "contactNumber" : "${MobileNo.text}",
        "nearBy" : "${Landmark.text}",
        "pincode" : "${Pincode.text}",
        "city" : "${City.text}",
        "addressLine" : "${Address.text}",
        "userId" : "${widget.userId}",
        "addressId" : "${widget.data['id']}"
      }),);
    print("REsponse : ${res.statusCode}");
    if (res.statusCode == 200){
      print("Address STATUS 200");
      print(res.body);
      Map<String,dynamic> addressResBody=await jsonDecode(res.body) as Map;
      if(addressResBody['status']==true){
        return true;
      }else{
        print("Response status false----------------");
        return false;
      }
    }else{
      print("Response status is not 200----------------");
      return false;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    print("Address Data := ${widget.data}");
    Fname=widget.data==null?TextEditingController():TextEditingController(text: widget.data['contact_name']);
    Address=widget.data==null?TextEditingController():TextEditingController(text: widget.data['address']);
    Landmark=widget.data==null?TextEditingController():TextEditingController(text: widget.data['near']);
    Pincode=widget.data==null?TextEditingController():TextEditingController(text: widget.data['pincode'].toString());
    //Area=widget.data==null?TextEditingController():TextEditingController(text: widget.data['contact_name']);
    City=widget.data==null?TextEditingController():TextEditingController(text: widget.data['city']);
    MobileNo=widget.data==null?TextEditingController():TextEditingController(text: widget.data['contact_no'].toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(
          key: _scaffoldKey,
          // resizeToAvoidBottomInset: false,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            title: Text(
              widget.title,
              style: TextStyle(color: Colors.lightGreen),
            ),
            //backgroundColor: Colors.white,
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(top: 10, left: 10, right: 10),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child: Text("ENTER FULL NAME*",)),
                  SizedBox(height: 5,),
                  TextFormField(
                    keyboardType: TextInputType.text,
                    controller: Fname,
                    //initialValue: widget.data==null?null:widget.data['contact_name'],
                    decoration: InputDecoration(
                      counterText: "",
                      isDense: true,
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child: Text("ADDRESS*",)),
                  SizedBox(height: 5,),
                  TextFormField(
                    controller: Address,
                    //initialValue: widget.data==null?null:widget.data['address'],
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      counterText: "",
                      isDense: true,
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child: Text("LANDMARK (Optonal)",)),
                  SizedBox(height: 5,),
                  TextFormField(
                    controller: Landmark,
                    //initialValue: widget.data==null?null:widget.data['near'],
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      counterText: "",
                      isDense: true,
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child: Text("PINCODE",)),
                  SizedBox(height: 5,),
                  TextFormField(
                    controller: Pincode,
                    //initialValue: widget.data==null?null:widget.data['pincode'].toString(),
                    maxLength: 6,
                    inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      counterText: "",
                      isDense: true,
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child: Text("AREA",)),
                  SizedBox(height: 5,),
                  TextFormField(
                    readOnly: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      counterText: "",
                      isDense: true,
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child: Text("CITY*",)),
                  SizedBox(height: 5,),
                  TextFormField(
                    controller: City,
                    //initialValue: widget.data==null?null:widget.data['city'],
                    //readOnly: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      counterText: "",
                      isDense: true,
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child: Text("STATE*",)),
                  SizedBox(height: 5,),
                  TextFormField(
                    initialValue: widget.data==null?null:'Gujarat',
                    readOnly: true,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      counterText: "",
                      isDense: true,
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child: Text("YOUR MOBILE NUMBER*",)),
                  SizedBox(height: 5,),
                  TextFormField(
                    controller: MobileNo,
                    //initialValue: widget.data==null?null:widget.data['contact_no'],
                    maxLength: 10,
                    inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      counterText: "",
                      prefixText: "+91 ",
                      isDense: true,
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: RaisedButton(
                      color: Colors.green,
                      child: Text(
                        "SAVE TO ADDRESS BOOK",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () async{
                        bool flag=await widget.title=="EDIT ADDRESS"?await updateAddress():await addNewAddress();
                        if(flag){
                          _scaffoldKey.currentState.showSnackBar(
                              SnackBar(
                                content: const Text("Address Successfully Updated!"),
                              )
                          );
                        }else{
                          showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context){
                              return AlertDialog(
                                title: Text("Error!"),
                                content: Text("Something Went Wrong!..Please try Later."),
                                contentPadding: EdgeInsets.all(10),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text("OK"),
                                    onPressed: (){
                                      Navigator.of(context).pop();
                                    },
                                  )
                                ],
                              );
                            },
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
