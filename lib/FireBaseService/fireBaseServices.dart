
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_shopping_app/SignIn.dart';
import 'package:flutter_shopping_app/main.dart';

class AuthServic{
  handleAuth(){
    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (BuildContext context,snapShot){
        // if(snapShot.hasData)
        //   return HomePage();
        // else{
        //   print("Signed Out .......");
        //   return HomePage();
        // }
      });
  }

  signOut(){
    FirebaseAuth.instance.signOut();
  }

  signIn(smsCode,verId){
    AuthCredential authCredential = PhoneAuthProvider.credential(verificationId: verId, smsCode: smsCode);
    FirebaseAuth.instance.signInWithCredential(authCredential).catchError((error){
      print(error);
    });
  }
}