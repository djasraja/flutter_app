import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/DataBaseSqlite/DBHelper.dart';
import 'package:flutter_shopping_app/Model/MyListModel.dart';
import 'package:flutter_shopping_app/ProductPage.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'BottomNavigationTab.dart';
import 'Model/CartModel.dart';
import 'main.dart';

class ItemListViewGetter extends StatefulWidget {
  List<dynamic> itemsList;
  ItemListViewGetter({@required this.itemsList});
  @override
  _ItemListViewGetterState createState() => _ItemListViewGetterState();
}

class _ItemListViewGetterState extends State<ItemListViewGetter> {
  //var itemListviewDetails=["assets/1.jpeg","assets/2.jpeg","assets/3.jpeg","assets/4.jpeg","assets/5.jpeg","assets/6.jpeg"];

  //String qwert='[{"image":"assets/1.jpeg","title":"Abc1"},{"image":"assets/2.jpeg","title":"Abc2"},{"image":"assets/3.jpeg","title":"Abc3"},{"image":"assets/4.jpeg","title":"Abc4"},{"image":"assets/5.jpeg","title":"Abc5"},{"image":"assets/6.jpeg","title":"Abc6"}]';

  List<CartModel> _cartItem = [];



  int _getItemIndexFromCart(int prod_id) {
    int index = -1;
    for (int i = 0; i < _cartItem.length; i++) {
      if (_cartItem[i].cartId == prod_id) {
        index = i;
        break;
      }
    }
    return index;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              "",
              style: TextStyle(color: Colors.lightGreen),
            ),
//            actions: <Widget>[
//              Container(
//                child: PopupMenuButton<popUpItems>(
//                  //onSelected: (){},
//                  tooltip: "Actions",
//                  itemBuilder: (BuildContext context) {
//                    return choicesBeforeLogin.map((popUpItems choice) {
//                      return PopupMenuItem<popUpItems>(
//                        value: choice,
//                        child: Container(
//                          //padding: EdgeInsets.all(4),
//                          child: Row(
//                            children: <Widget>[
//                              Icon(choice.icon),
////                              SizedBox(
////                                width: 5,
////                              ),
//                              Text(choice.values)
//                            ],
//                          ),
//                        ),
//                      );
//                    }).toList();
//                  },
//                ),
//              )
//            ],
            //backgroundColor: Colors.white,
          ),
          bottomNavigationBar: BottomNavigationTab(),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                FutureBuilder(
                  future: DBHelper().getCartItems(),
                  builder: (context, snapShot) {
                    if (snapShot.hasData) _cartItem = snapShot.data;
                    return ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: widget.itemsList.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            print("$index Container pressed");
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => ProductPage(
                                  data: widget.itemsList[index],
                                ),
                              ),
                            );
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      width: 2, color: Colors.green)),
                            ),
                            //decoration: index % 2 == 0 ? BoxDecoration(color: Colors.orange) :BoxDecoration(color: Colors.deepOrangeAccent) ,
                            height: 160,
                            margin: EdgeInsets.all(5),
                            child: Row(
                              children: <Widget>[
                                SizedBox(
                                  width: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image.network(
                                      widget.itemsList[index]['img1'],
                                      fit: BoxFit.fill,
                                      height: 90,
                                      width: MediaQuery.of(context).size.width *
                                          0.20,
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        addToList(
                                            widget.itemsList[index]['id']);
                                        _showToast(context);
                                      },
                                      child: Text(
                                        "ADD TO LIST",
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.70,
                                  //color: Colors.red,
                                  child: Column(
                                    children: <Widget>[
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Container(
                                          padding: EdgeInsets.all(5),
                                          //color: Colors.blue,
                                          height: 45,
                                          child: Text(
                                            "${widget.itemsList[index]['title']} : ${widget.itemsList[index]['unit']} ${widget.itemsList[index]['unit_title']}",
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.centerRight,
                                        child: Container(
                                          //color: Colors.green,
                                          height: 60,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: <Widget>[
                                              Text(
                                                "MRP : \u{20B9}${widget.itemsList[index]['price']}",
                                                style: TextStyle(
                                                  color: Colors.black87,
                                                  decoration: TextDecoration
                                                      .lineThrough,
                                                ),
                                              ),
                                              Text(
                                                "ZPrice : \u{20B9}${widget.itemsList[index]['sales_price']}",
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontSize: 18),
                                              ),
                                              Text(
                                                "Save : \u{20B9}${widget.itemsList[index]['price'] - widget.itemsList[index]['sales_price']}",
                                                style: TextStyle(
                                                    color: Colors.red,
                                                    fontSize: 15),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.center,
                                        child: Container(
                                          //padding: EdgeInsets.all(5),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                padding: EdgeInsets.all(8),
                                                height: 35,
                                                width: 100,
                                                decoration: BoxDecoration(
                                                    border:
                                                        Border.all(width: 1),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                5))),
                                                child: Text(
                                                  "${widget.itemsList[index]['unit']} ${widget.itemsList[index]['unit_title']}",
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                              SizedBox(width: 20),
                                              (_getItemIndexFromCart(widget.itemsList[index]['id']) < 0) ? ((widget.itemsList[index]['qty']>0)?RaisedButton(
                                                    onPressed: () async {
                                                    print("Add to cart");
                                                    await addToCart(widget.itemsList[index]['id'], context);
                                                    setState(() {});
                                                    },
                                                    color: Colors.green,
                                                    textColor: Colors.white,
                                                    child:
                                                    Text("ADD TO CART"),
                                                  ):RaisedButton(onPressed: (){},color: Colors.redAccent, child: Text("OUT OF STOCK", style: TextStyle(color: Colors.white),),)
                                              ) : Container(
                                                //padding: EdgeInsets.all(5),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: <Widget>[
                                                    Container(
                                                      alignment: Alignment.center,
                                                      width: 40,
                                                      height: 30,
                                                      decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60),topLeft: Radius.circular(60),)),
                                                      child: RaisedButton(
                                                        onPressed: () async{
                                                          print("Minus");
                                                          int i=_getItemIndexFromCart(widget.itemsList[index]['id']);
                                                          CartModel model=CartModel();
                                                          var dbHelper=DBHelper();
                                                          model.id=_cartItem[i].id;
                                                          model.cartId=_cartItem[i].cartId;
                                                          if(_cartItem[i].qty == 1){
                                                            await dbHelper.deleteFromCart(model);
                                                          }else{
                                                            model.qty = _cartItem[i].qty - 1;
                                                            await dbHelper.updateCart(model);
                                                          }
                                                          setState(() {});
                                                        },
                                                        color: Colors.white,
                                                        textColor: Colors.green,
                                                        child: Center(child: Text("-",style: TextStyle(fontSize: 20),)),
                                                      ),
                                                    ),
                                                    SizedBox(width: 5,),
                                                    Container(child: Text("${_cartItem[_getItemIndexFromCart(widget.itemsList[index]['id'])].qty}",style: TextStyle(fontSize: 20),)),
                                                    SizedBox(width: 5,),
                                                    Container(
                                                      alignment: Alignment.center,
                                                      width: 40,
                                                      height: 30,
                                                      decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomRight: Radius.circular(60),topRight: Radius.circular(60),)),
                                                      child: RaisedButton(
                                                        onPressed: () async {
                                                          print("Plus");
                                                          int i=_getItemIndexFromCart(widget.itemsList[index]['id']);
                                                          if(((_cartItem[i].qty + 1) <= widget.itemsList[index]['qty']) && ((_cartItem[i].qty + 1) <= widget.itemsList[index]['sell_qty'])){
                                                            CartModel model=CartModel();
                                                            var dbHelper=DBHelper();
                                                            model.id=_cartItem[i].id;
                                                            model.cartId=_cartItem[i].cartId;
                                                            model.qty = _cartItem[i].qty + 1;
                                                            await dbHelper.updateCart(model);
                                                          }else{
                                                            Scaffold.of(context).showSnackBar(
                                                              SnackBar(
                                                                content: const Text('Reached maximum limit of product'),
                                                              ),
                                                            );
                                                          }
                                                          setState(() {});
                                                        },
                                                        color: Colors.white,
                                                        textColor: Colors.green,
                                                        child: Center(child: Text("+",style: TextStyle(fontSize: 20),)),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void addToCart(int id, BuildContext context) {
    CartModel list = new CartModel();
    list.cartId = id;
    list.qty = 1;
    var dbHelper = DBHelper();
    dbHelper.addNewToCart(list);
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Added to Cart'),
//        action: SnackBarAction(
//            label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  void addToList(int id) {
    MyListModel list = new MyListModel();
    list.listId = id;
    var dbHelper = DBHelper();
    dbHelper.addNewToList(list);
  }

  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Added to MY LIST'),
//        action: SnackBarAction(
//            label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
}
