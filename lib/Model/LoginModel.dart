// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) => LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    this.message,
    this.status,
    this.token,
  });

  String message;
  bool status;
  String token;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
    message: json["message"],
    status: json["status"],
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "status": status,
    "token": token,
  };
}
