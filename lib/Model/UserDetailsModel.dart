// To parse this JSON data, do
//
//     final userDetailsModel = userDetailsModelFromJson(jsonString);

import 'dart:convert';

UserDetailsModel userDetailsModelFromJson(String str) => UserDetailsModel.fromJson(json.decode(str));

String userDetailsModelToJson(UserDetailsModel data) => json.encode(data.toJson());

class UserDetailsModel {
  UserDetailsModel({
    this.username,
    this.firstName,
    this.lastName,
    this.phone,
    this.altrPhone,
    this.email,
    this.id,
    this.password,
  });

  String username;
  String firstName;
  String lastName;
  int phone;
  int altrPhone;
  String email;
  int id;
  String password;

  factory UserDetailsModel.fromJson(Map<String, dynamic> json) => UserDetailsModel(
    username: json["username"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    phone: json["phone"],
    altrPhone: json["altr_phone"],
    email: json["email"],
    id: json["id"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "username": username,
    "firstName": firstName,
    "lastName": lastName,
    "phone": phone,
    "altr_phone": altrPhone,
    "email": email,
    "id": id,
    "password": password,
  };
}
