import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/AddressBook.dart';
import 'package:flutter_shopping_app/ChangePassword.dart';
import 'package:flutter_shopping_app/MyProfile.dart';
import 'package:flutter_shopping_app/theme.dart';

class MyAccount extends StatelessWidget {

  List<String> MyAccountDetails=["MY PROFILE","ADDRESS BOOK","MY ORDERS","CHANGE PASSWORD"];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            title: Text(
              "My Account",
              style: TextStyle(color: Colors.lightGreen),
            ),
            //backgroundColor: Colors.white,
          ),
          body: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                border: Border.all(width: 1,color: Colors.grey,),
              ),
              child: Column(
                children: <Widget>[
                  for(int i=0;i<MyAccountDetails.length;i++) RaisedButton(
                    color: Colors.white54,
                    elevation: 0,
                    onPressed:  () {if(MyAccountDetails[i]=="MY PROFILE") Navigator.push(context, MaterialPageRoute(builder: (_) => MyProfile(),),);
                                else if(MyAccountDetails[i]=="ADDRESS BOOK") Navigator.push(context, MaterialPageRoute(builder: (_) => AddressBook(),),);
                                else if(MyAccountDetails[i]=="MY ORDERS") print("MY ORDERS Pressed");
                                else if(MyAccountDetails[i]=="CHANGE PASSWORD") Navigator.push(context, MaterialPageRoute(builder: (_) => ChangePassword(),),);
                    },
                    child: Container(
                      padding: EdgeInsets.all(10),
                      decoration:BoxDecoration(
                        border: i != MyAccountDetails.length-1 ? Border(bottom: BorderSide(color: Colors.grey,width: 2)) : null,
                    ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(MyAccountDetails[i],style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                          Icon(Icons.arrow_forward_ios,color: Colors.grey,size: 18,),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
