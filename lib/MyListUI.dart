import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/DataBaseSqlite/DBHelper.dart';
import 'package:flutter_shopping_app/Model/MyListModel.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:flutter_shopping_app/http_services.dart';
import 'package:http/http.dart' as http;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'BottomNavigationTab.dart';
import 'Model/CartModel.dart';
import 'ProductPage.dart';



Future<List<MyListModel>> getMyListFromDB() async{
  print("ggteetttttttttttttttt");
  var dbHelper=DBHelper();
  Future<List<MyListModel>> myList=dbHelper.getList();
  return myList;
}

Future<List<CartModel>> getCartItemsFromDB() async {
  print("ggteetttttttttttttttt CART");
  var dbHelper = DBHelper();
  Future<List<CartModel>> cartItem = dbHelper.getCartItems();
  return cartItem;
}

class MyListUI extends StatefulWidget {
  @override
  _MyListUIState createState() => _MyListUIState();
}

class _MyListUIState extends State<MyListUI> {
  List myListProducts=[];

  List<CartModel> _cartItemFromDb = [];

  Future getCartItems() async{
    _cartItemFromDb.clear();
    List<CartModel> cartItem = await getCartItemsFromDB();
    setState(() {
      _cartItemFromDb = cartItem;
    });

    print("${_cartItemFromDb.length} xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
  }

  int _getItemIndexFromCart(int prod_id) {
    int index = -1;
    print("${_cartItemFromDb.length} --------------------------------");
    for (int i = 0; i < _cartItemFromDb.length; i++) {
      if (_cartItemFromDb[i].cartId == prod_id) {
        index = i;
        break;
      }
    }
    return index;
  }

  Future getMyList() async{
    Map<String,dynamic> dataItems;
    myListProducts.clear();
    List listFromDb=await getMyListFromDB();
    //print("ggtee2222222222222222222222222=${listFromDb.length} ${listFromDb[0].listId}");
    for(int i=0;i<listFromDb.length;i++){

      print(getProductByIdUrl+"${listFromDb[i].listId}");
      print(listFromDb[i].listId);
      http.Response res = await http.get(getProductByIdUrl+"${listFromDb[i].listId}");
      if(res.statusCode == 200){
        print("STATUS 200");
        dataItems=jsonDecode(res.body);
        setState(() {
          myListProducts.add(dataItems);
        });
      }
    }
    print(myListProducts.length);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMyList();
    getCartItems();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text("MY LIST",style: TextStyle(color: Colors.lightGreen),),
            leading: IconButton(icon: Icon(Icons.arrow_back_ios),onPressed: (){Navigator.pop(context);},),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                myListProducts.isNotEmpty ? ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: myListProducts == null? false:myListProducts.length,
                    itemBuilder: (context, index){
                      return GestureDetector(
                        onTap: () {
                          print("$index Container pressed");
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => ProductPage(
                                data: myListProducts[index],
                              ),
                            ),
                          );
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: 2, color: Colors.green)),
                          ),
                          //decoration: index % 2 == 0 ? BoxDecoration(color: Colors.orange) :BoxDecoration(color: Colors.deepOrangeAccent) ,
                          //height: 160,
                          margin: EdgeInsets.all(5),
                          child: Row(
                            children: <Widget>[
                              SizedBox(width: 10,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.network(myListProducts[index]['img1'],fit: BoxFit.fill,height: 60,width: MediaQuery.of(context).size.width * 0.20,),
                                ],
                              ),
                              SizedBox(width: 10,),
                              Container(
                                width:
                                MediaQuery.of(context).size.width *
                                    0.65,
                                //color: Colors.red,
                                child: Column(
                                  children: <Widget>[
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        //color: Colors.blue,
                                        //height: 30,
                                        child: Text(
                                          "${myListProducts[index]['title']} : ${myListProducts[index]['unit']} ${myListProducts[index]['unit_title']}",
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: Container(
                                        //color: Colors.green,
                                        height: 60,
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Text(
                                              "MRP : \u{20B9}${myListProducts[index]['price']}",
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.black87,
                                                decoration:
                                                TextDecoration
                                                    .lineThrough,
                                              ),
                                            ),
                                            Text(
                                              "ZPrice : \u{20B9}${myListProducts[index]['sales_price']}",
                                              style: TextStyle(
                                                  color: Colors.green,
                                                  fontSize: 16),
                                            ),
                                            Text(
                                              "Save : \u{20B9}${myListProducts[index]['price'] - myListProducts[index]['sales_price']}",
                                              style: TextStyle(
                                                  color: Colors.red,
                                                  fontSize: 13),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Container(
                                        //padding: EdgeInsets.all(5),
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          children: <Widget>[
                                            IconButton(icon: Icon(MdiIcons.trashCanOutline),
                                              color: Colors.grey,
                                              onPressed: () {
                                                MyListModel model=MyListModel();
                                                model.listId=myListProducts[index]['id'];
                                                var dbHelper=DBHelper();
                                                dbHelper.deleteFromList(model);
                                                _showToast(context);
                                                setState(() {
                                                  getMyList();
                                                });
                                              },),
                                            (_getItemIndexFromCart(myListProducts[index]['id']) < 0)?((myListProducts[index]['qty']>0)?RaisedButton(
                                              onPressed: () {
                                                print("ADD TO CART");
                                                addToCart(myListProducts[index]['id'], context);
                                              },
                                              color: Colors.green,
                                              textColor: Colors.white,
                                              child:
                                              Text("ADD TO CART"),
                                            ):RaisedButton(onPressed: () {print("ADD TO CART");}, color: Colors.red, textColor: Colors.white, child: Text("OUT OF STOCK"),)
                                            ):RaisedButton(
                                              onPressed: () {
                                                print("ADD TO CART");
                                              },
                                              color: Colors.black26,
                                              textColor: Colors.white,
                                              child:
                                              Text("ADD TO CART"),
                                            ),

                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    }
                ): Container(height: MediaQuery.of(context).size.height,child: Center(child: Text("The list is empty :(",style: TextStyle(fontSize: 20),textAlign: TextAlign.center,)),),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void addToCart(int id, BuildContext context) {
    CartModel list = new CartModel();
    list.cartId = id;
    list.qty = 1;
    var dbHelper = DBHelper();
    dbHelper.addNewToCart(list);
  }
  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Deleted from List'),
      ),
    );
  }
}
