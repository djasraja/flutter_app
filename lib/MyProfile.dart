import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_shopping_app/Model/LoginModel.dart';
import 'package:flutter_shopping_app/Model/UserDetailsModel.dart';
import 'package:flutter_shopping_app/http_services.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:http/http.dart' as http;

import 'DataBaseSqlite/DBHelper.dart';

class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {


  var fname;
  var lname;
  var altPhonr;
  var email;

  // String fname;
  // String lname;
  // String altPhonr;
  // String email;

  Future<List> _getProfileDetails()async{
    List MyProfileDetails=[];
    MyProfileDetails=await DBHelper().getUserDetails();
    print("Future : ${MyProfileDetails}");
    return MyProfileDetails;
  }

  @override
  void initState() {
    // TODO: implement initState
    _getProfileDetails().then((value) {
      fname=new TextEditingController(text:value[0].firstName);
      lname=new TextEditingController(text: value[0].lastName);
      altPhonr=new TextEditingController(text:value[0].altrPhone.toString()=="null"?"":value[0].altrPhone.toString());
      email=new TextEditingController(text: value[0].email=="null"?"":value[0].email);
    });
    super.initState();
  }

  @override
  // void dispose() {
  //   // TODO: implement dispose
  //   fname.dispose();
  //   lname.dispose();
  //   email.dispose();
  //   altPhonr.dispose();
  //   super.dispose();
  // }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(
          // resizeToAvoidBottomInset: false,
          // resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            title: Text(
              "MY PROFILE",
              style: TextStyle(color: Colors.lightGreen),
            ),
            //backgroundColor: Colors.white,
          ),
          body: FutureBuilder(
            future: _getProfileDetails(),
            builder: (context, snapshot) {
              if(snapshot.hasData) print("Logged In User Data${snapshot.data[0].altrPhone}");
              return snapshot.hasData?SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(top: 20, left: 10, right: 10),
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 10,),
                      Align(alignment: Alignment.topLeft,child: Text("FIRST NAME*",)),
                      SizedBox(height: 5,),
                      TextFormField(
                        controller: fname,
                        //onChanged: (val){fname=val;},
                        //initialValue: snapshot.data[0].firstName,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          counterText: "",
                          //isDense: true,
                          contentPadding: EdgeInsets.all(8),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.green),
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child:Text("LAST NAME*",)),
                      SizedBox(height: 5,),
                      TextFormField(
                        controller: lname,
                        //onChanged: (val){lname=val;},
                       // initialValue: snapshot.data[0].lastName,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          counterText: "",
                          //isDense: true,
                          contentPadding: EdgeInsets.all(8),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.green),
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child:Text("YOUR ALTERNATE MOBILE NUMBER",)),
                      SizedBox(height: 5,),
                      TextFormField(
                        controller: altPhonr,
                        //onChanged: (val){altPhonr=val;},
                        //initialValue: snapshot.data[0].altrPhone==0?"":snapshot.data[0].altrPhone.toString(),
                        maxLength: 10,
                        inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          counterText: "",
                          prefixText: "+91 ",
                          //isDense: true,
                          contentPadding: EdgeInsets.all(8),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                  Align(alignment: Alignment.topLeft,child:Text("YOUR EMAIL ID",)),
                      SizedBox(height: 5,),
                      TextFormField(
                        controller: email,
                        //onChanged: (val){email=val;},
                        //initialValue: snapshot.data[0].email=="null"?"":snapshot.data[0].email,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          counterText: "",
                          //isDense: true,
                          contentPadding: EdgeInsets.all(8),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                      SizedBox(
                        height: 50,
                        width: double.infinity,
                        child: RaisedButton(
                          color: Colors.green,
                          child: Text(
                            "SAVE CHANGES",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          onPressed: () async{
                            bool flag=await _updateProfile(snapshot.data[0].id);
                            if(flag){
                              Scaffold.of(context).showSnackBar(
                                  SnackBar(
                                    content: const Text("Profile Successfully Updated!"),
                                  )
                              );
                            }else{
                              showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context){
                                  return AlertDialog(
                                    title: Text("Error!"),
                                    content: Text("Something Went Wrong!..Please try Later."),
                                    contentPadding: EdgeInsets.all(10),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text("OK"),
                                        onPressed: (){
                                          Navigator.of(context).pop();
                                        },
                                      )
                                    ],
                                  );
                                },
                              );
                            }
                          },
                        ),
                      ),
                    ],
                  )
                ),
              ):Center(child: CircularProgressIndicator(),);
            }
          ),
        ),
      ),

    );
  }

  Future<bool> _updateProfile(int userid)async{
    print(postUpdateProfileUrl);
    print("${fname.text} ${lname.text} ${altPhonr.text} ${email.text}");
    http.Response res=await http.post(postUpdateProfileUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "userId" : "${userid}",
        "firstName" : "${fname.text}",
        "lastName" : "${lname.text}",
        "altrPhone" : "${altPhonr.text}",
        "email" : "${email.text}"
      }),);
    print("REsponse : ${res.statusCode}");
    if(res.statusCode == 200){
      Map<String, dynamic> list=jsonDecode(res.body) as Map<String,dynamic>;
      print(list);
      if(list['status']==true){
        List<LoginModel> loginTableData=new List<LoginModel>();
        loginTableData=await DBHelper().getLoginConfirmation();
        http.Response refreshTokenRes=await http.post(postRefreshTokenUrl,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            "token" : "${loginTableData[0].token}"
          }),);
        if(refreshTokenRes.statusCode==200){
          Map<String, dynamic> refreshTokenList=jsonDecode(refreshTokenRes.body) as Map<String,dynamic>;
          if(refreshTokenList['status']==true){
            await DBHelper().deleteFromLogin();
            print("-------------Deleted From Login");
            await DBHelper().addToLogin(await loginModelFromJson(refreshTokenRes.body));
            print("-------------updated login tabl after refresh");
            //to get the updated the data of user and store it in local DB
            http.Response userVerifyViaTokenResponse=await http.post(postUserVerifyViaTokenUrl,
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
              },
              body: jsonEncode(<String, String>{
                "token" : "${refreshTokenList['token']}"
              }),);
            print("userVerifyViaTokenResponse : ${userVerifyViaTokenResponse.statusCode}");
            print("${refreshTokenList['token']}");
            print("${refreshTokenList}");
            print("${userVerifyViaTokenResponse.body}");
            if(userVerifyViaTokenResponse.statusCode==200){
              print("Local DB Updated for userDetails");
              await DBHelper().deleteFromUserDetails();
              print("-------------Deleted From User Details Table");
              await DBHelper().addToUserDetails(userDetailsModelFromJson(userVerifyViaTokenResponse.body));
              print("-------------updated user details tabl after refresh");
              return true;
            }else{
              print("=================Failed at userVerifyViaTokenResponse");
              return false;
            }

          }else{
            print("=================Failed at Refresh token NOT TRUE");
            return false;
          }
        }else{
          print("=================Failed at Refresh token STATUS");
          return false;
        }
        return true;
      }else{
        print("=====================Failed at update profile NOT TRUE");
        return false;
      }
    }else{
      print("=====================Failed at update profile STATUS");
      return false;
    }

  }

}
