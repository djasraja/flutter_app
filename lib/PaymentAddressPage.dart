import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/PaymentChoicePage.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'DataBaseSqlite/DBHelper.dart';
import 'EditAddAddressPage.dart';
import 'Model/UserDetailsModel.dart';
import 'http_services.dart';
import 'package:http/http.dart' as http;

class PaymentAddressPage extends StatefulWidget {
  double totalAmount,totalSaving;
  @override
  _PaymentAddressPageState createState() => _PaymentAddressPageState();

  PaymentAddressPage({@required this.totalAmount,@required this.totalSaving});
}

class _PaymentAddressPageState extends State<PaymentAddressPage> {
  List AddressDetails;
  List<UserDetailsModel> userDetailsModel=[];
  Future getAddressDetails() async {
    List dataItems;
    userDetailsModel=await DBHelper().getUserDetails();
    print("${getAddressDetailsUrl}${userDetailsModel[0].id}");
    print("Json Address getter");
    http.Response res = await http.get("${getAddressDetailsUrl}${userDetailsModel[0].id}");
    print("---------------------------${res.statusCode}");
    if(res.statusCode == 200){
      dataItems=jsonDecode(res.body);
      print(dataItems);
      setState(() {
        AddressDetails=dataItems;
      });
    }else{
      print("Json Data is NOT");
    }
  }

  Future deleteAddressByID(int AddressId,BuildContext context) async{
    print("Address ID  : ${AddressId} USer ID : ${userDetailsModel[0].id}");
    print(postDeleteAddressUrl);
    http.Response res =await http.post(postDeleteAddressUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "addressId" : "${AddressId}",
        "userId" : "${userDetailsModel[0].id}"
      }),);
    print("${res.statusCode}");
    if(res.statusCode==200){
      Map<String,dynamic> resList=jsonDecode(res.body) as Map<String,dynamic>;
      if(resList['status']==true){
        Scaffold.of(context).showSnackBar(
            SnackBar(
              content: const Text("Address Deleted Successfully!"),
            )
        );
        setState(() {
          print("Address Deleted successfully!");
        });
      }else{
        Scaffold.of(context).showSnackBar(
            SnackBar(
              content: const Text("Something Went wrong!Try Later"),
            )
        );
        print("Something Went wrong! Status is FALSE");
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AddressDetails!=null?AddressDetails.clear():print("Nothing to clear");
    userDetailsModel!=null?userDetailsModel.clear():print("Nothing to clear");
    print("INIIIIIIIIIItt Adddress");
    getAddressDetails();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            title: Text(
              "SELECT ADDRESS",
              style: TextStyle(color: Colors.lightGreen),
            ),
            //backgroundColor: Colors.white,
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
              child: Column(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: AddressDetails == null ?0 : AddressDetails.length,
                    itemBuilder: (contect,index) {
                      return GestureDetector(
                        onTap: (){
                          print("${AddressDetails[index]['id']}");
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => PaymentChoicePage(totalAmount: widget.totalAmount,totalSaving: widget.totalSaving,AddressID: AddressDetails[index]['id'],)));
                        },
                        child: Container(
                          margin: EdgeInsets.all(5),
                          decoration: BoxDecoration(border: Border.all(width: 1,color: Colors.grey),borderRadius: BorderRadius.all(Radius.circular(8)),color: Colors.white),
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(AddressDetails[index]['contact_name']),
                              SizedBox(height: 2,),
                              Text(AddressDetails[index]['address']),
                              SizedBox(height: 2,),
                              Text("${AddressDetails[index]['city']}, Gujarat"),
                              SizedBox(height: 2,),
                              Text(AddressDetails[index]['pincode'].toString(),style: TextStyle(fontWeight: FontWeight.bold),),
                              SizedBox(height: 5,),
                              Text("Landmark: ${AddressDetails[index]['near']}"),
                              SizedBox(height: 5,),
                              Row(
                                children: <Widget>[
                                  IconButton(icon: Icon(MdiIcons.trashCanOutline), color: Colors.grey,onPressed: () async{ await deleteAddressByID(AddressDetails[index]['id'],contect).then((value) async {await getAddressDetails();setState(() {});}); },),
                                  IconButton(icon: Icon(MdiIcons.pencilOutline),color: Colors.grey, onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (_) => EditAddAddress(title: "EDIT ADDRESS",data: AddressDetails[index],userId: userDetailsModel[0].id,),),); },),
                                ],),
                            ],
                          ),
                        ),
                      );
                    },
                  ),


                  SizedBox(height: 10,),
                  SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        side: BorderSide(width: 1,color: Colors.black54),
                      ),
                      color: Colors.white,
                      child: Text(
                        "ADD NEW ADDRESS",
                        style: TextStyle(
                          color: Colors.green,
                        ),
                      ),
                      onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (_) => EditAddAddress(title: "ADD NEW ADDRESS",data: null,userId: userDetailsModel[0].id,),),);},
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
