import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/ConfirmAndOrderDetailsPage.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class PaymentChoicePage extends StatefulWidget {
  double totalAmount,totalSaving;
  int AddressID,TimeSlot=4;
  @override
  _PaymentChoicePageState createState() => _PaymentChoicePageState();

  PaymentChoicePage(
      {@required this.totalAmount,@required this.totalSaving,@required this.AddressID,@required this.TimeSlot});
}

class _PaymentChoicePageState extends State<PaymentChoicePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: basicTheme(),
        home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
          },
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
              title: Text(
                "PAYMENT MODE",
                style: TextStyle(color: Colors.lightGreen),
              ),
              //backgroundColor: Colors.white,
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("PAY ON DELIVERY",style: TextStyle(color: Colors.black45,fontSize: 18),textAlign: TextAlign.left,),
                    SizedBox(height: 10,),
                    RaisedButton(
                      elevation: 5,
                      color: Colors.white,
                      onPressed: (){print("COD");Navigator.of(context).push((MaterialPageRoute(builder: (context) => ConfirmAndOrderDetailsPage(totalAmount: widget.totalAmount,totalSaving: widget.totalSaving,AddressID: widget.AddressID, TimeSlot: widget.TimeSlot,))));},
                      child: Container(
                        padding: EdgeInsets.all(10),
                        //color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(MdiIcons.walletOutline,color: Colors.green,size: 30,),
                            SizedBox(width: 10,),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("CASH ON DELIVERY",style: TextStyle(color: Colors.black45,fontSize: 16),textAlign: TextAlign.left,),
                                SizedBox(height: 5,),
                                Text("Pay at the time of Delivery to our delivery partner.",style: TextStyle(color: Colors.black45,fontSize: 13),textAlign: TextAlign.left,),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
    );
  }
}
