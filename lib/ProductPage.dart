import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/main.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:marquee/marquee.dart';

import 'DataBaseSqlite/DBHelper.dart';
import 'Model/CartModel.dart';
//final GlobalKey<HomePageState> HomePageGlobalKeyBottomBar = new GlobalKey<HomePageState>();
class ProductPage extends StatefulWidget {
  Map<String, dynamic> data;

  @override
  _ProductPageState createState() => _ProductPageState();

  ProductPage({@required this.data});
}

Future<List<CartModel>> getCartItemsFromDB() async {
  print("ggteetttttttttttttttt CART");
  var dbHelper = DBHelper();
  Future<List<CartModel>> cartItem = dbHelper.getCartItems();
  return cartItem;
}

class _ProductPageState extends State<ProductPage> {

  List<CartModel> _cartItemFromDb = [];

  Future getCartItems() async{
    _cartItemFromDb.clear();
    List<CartModel> cartItem = await getCartItemsFromDB();
    setState(() {
      _cartItemFromDb = cartItem;
    });

    print("${_cartItemFromDb.length} xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
  }

  int _getItemIndexFromCart(int prod_id) {
    int index = -1;
    print("${_cartItemFromDb.length} --------------------------------");
    for (int i = 0; i < _cartItemFromDb.length; i++) {
      if (_cartItemFromDb[i].cartId == prod_id) {
        index = i;
        break;
      }
    }
    return index;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCartItems();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          setState(() {
            //HomePageGlobalKeyBottomBar.currentState.BottomBar=true;
          });
          Navigator.pop(context);
        },
        child: _cartItemFromDb.isNotEmpty ? Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              '${widget.data['title']}',
              style: TextStyle(color: Colors.green),
            ),
//            actions: <Widget>[
//              IconButton(icon: Icon(Icons.search), onPressed: () {})
//            ],
            //backgroundColor: Colors.white,
          ),
          bottomNavigationBar: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  onPressed: () {},
                  color: Colors.white,
                  textColor: Colors.green,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.31,
                    child: Text('ADD TO LIST',textAlign: TextAlign.center,),
                  ),
                ),
                (_getItemIndexFromCart(widget.data['id']) < 0) ? ((widget.data['qty']>0)?RaisedButton(
                  onPressed: () async {
                    print("Add to cart");
                    await addToCart(widget.data['id'], context);
                    setState(() {getCartItems();});
                  },
                  color: Colors.green,
                  textColor: Colors.white,
                  child:
                  Container(child: Text("ADD TO CART",textAlign: TextAlign.center,),width: MediaQuery.of(context).size.width * 0.53,),
                ):RaisedButton(onPressed: (){},color: Colors.redAccent, child: Container(width: MediaQuery.of(context).size.width * 0.53,child: Text("OUT OF STOCK",textAlign: TextAlign.center, style: TextStyle(color: Colors.white),)),)
                ):RaisedButton(
                  onPressed: (){},
                  color: Colors.black26,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.53,
                    child: Text('ADD TO CART',textAlign: TextAlign.center,style: TextStyle(color: Colors.white),),
                  ),
                ),
              ],
            ),
          ),
          body: SingleChildScrollView(
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 5, right: 10, left: 10),
                    height: MediaQuery.of(context).size.height * 0.4,
//                    color: Colors.orangeAccent,
                    child: Carousel(
                      dotBgColor: Colors.transparent,
                      dotIncreasedColor: Colors.blueGrey,
                      dotColor: Colors.black12,
                      //boxFit: BoxFit.none,
                      autoplay: false,
                      images: [
                        Image.network(
                          widget.data['img1'],
                        ),
                        if (widget.data['img2'] != null)
                          Image.network(
                            widget.data['img2'],
                          ),
                        if (widget.data['img3'] != null)
                          Image.network(
                            widget.data['img3'],
                          ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5, left: 10, right: 10),
                    decoration: BoxDecoration(
                        //color: Colors.blue,
                        border: Border(
                            top: BorderSide(width: 2, color: Colors.grey))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(5),
                              //color: Colors.green,
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: Text(
                                '${widget.data['title']} : ${widget.data['unit']} ${widget.data['unit_title']}',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              padding: EdgeInsets.all(8),
                              height: 35,
                              width: 100,
                              decoration: BoxDecoration(
                                  border: Border.all(width: 1),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                              child: Text(
                                "${widget.data['unit']} ${widget.data['unit_title']}",
                                textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                          crossAxisAlignment: CrossAxisAlignment.start,
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 15),
                          //color: Colors.green,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                "MRP : \u{20B9}${widget.data['price']}",
                                style: TextStyle(
                                    color: Colors.black87,
                                    decoration: TextDecoration.lineThrough,
                                    fontSize: 13),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "ZPrice : \u{20B9}${widget.data['sales_price']}",
                                style: TextStyle(
                                    color: Colors.green, fontSize: 19),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Your Savings : \u{20B9}${widget.data['price'] - widget.data['sales_price']}",
                                style: TextStyle(
                                    color: Colors.orangeAccent, fontSize: 15),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              (_getItemIndexFromCart(widget.data['id']) >= 0 && widget.data['qty'] > 0)?Container(
                                //padding: EdgeInsets.all(5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.center,
                                      width: 40,
                                      height: 30,
                                      decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60),topLeft: Radius.circular(60),)),
                                      child: RaisedButton(
                                        onPressed: () async{
                                          print("Minus");
                                          int i=_getItemIndexFromCart(widget.data['id']);
                                          CartModel model=CartModel();
                                          var dbHelper=DBHelper();
                                          model.id=_cartItemFromDb[i].id;
                                          model.cartId=_cartItemFromDb[i].cartId;
                                          if(_cartItemFromDb[i].qty == 1){
                                            await dbHelper.deleteFromCart(model);
                                          }else{
                                            model.qty = _cartItemFromDb[i].qty - 1;
                                            await dbHelper.updateCart(model);
                                          }
                                          setState(() {getCartItems();});
                                        },
                                        color: Colors.white,
                                        textColor: Colors.green,
                                        child: Center(child: Text("-",style: TextStyle(fontSize: 20),)),
                                      ),
                                    ),
                                    SizedBox(width: 5,),
                                    Container(child: Text("${_cartItemFromDb[_getItemIndexFromCart(widget.data['id'])].qty}",style: TextStyle(fontSize: 20),)),
                                    SizedBox(width: 5,),
                                    Container(
                                      alignment: Alignment.center,
                                      width: 40,
                                      height: 30,
                                      decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomRight: Radius.circular(60),topRight: Radius.circular(60),)),
                                      child: RaisedButton(
                                        onPressed: () async {
                                          print("Plus");
                                          int i=_getItemIndexFromCart(widget.data['id']);
                                          if(((_cartItemFromDb[i].qty + 1) <= widget.data['qty']) && ((_cartItemFromDb[i].qty + 1) <= widget.data['sell_qty'])){
                                            CartModel model=CartModel();
                                            var dbHelper=DBHelper();
                                            model.id=_cartItemFromDb[i].id;
                                            model.cartId=_cartItemFromDb[i].cartId;
                                            model.qty = _cartItemFromDb[i].qty + 1;
                                            await dbHelper.updateCart(model);
                                          }else{
                                            Scaffold.of(context).showSnackBar(
                                              SnackBar(
                                                content: const Text('Reached maximum limit of product'),
                                              ),
                                            );
                                          }
                                          setState(() {getCartItems();});
                                        },
                                        color: Colors.white,
                                        textColor: Colors.green,
                                        child: Center(child: Text("+",style: TextStyle(fontSize: 20),)),
                                      ),
                                    ),
                                  ],
                                ),
                              ):Container(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Colors.grey, width: 1)),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: Colors.green, width: 2)),
                      ),
                      child: Text(
                        'Description',
                        style: TextStyle(fontSize: 20, color: Colors.green),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.all(10),
                    child: Text(
                      '${widget.data['intro']}',
                      style: TextStyle(color: Colors.black54, fontSize: 18),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ): Center(child: CircularProgressIndicator()),
      );
  }
  void addToCart(int id, BuildContext context) {
    CartModel list = new CartModel();
    list.cartId = id;
    list.qty = 1;
    var dbHelper = DBHelper();
    dbHelper.addNewToCart(list);
  }
}
