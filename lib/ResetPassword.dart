import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/SignIn.dart';
import 'package:flutter_shopping_app/http_services.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:http/http.dart' as http;

class ResetPassword extends StatefulWidget {
  String userName;
  ResetPassword({@required this.userName});

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  bool _obscuredText = true;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String password,confirmPassword;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(
          // resizeToAvoidBottomInset: false,
          // resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            title: Text(
              "Reset Password",
              style: TextStyle(color: Colors.green),
            ),
            //backgroundColor: Colors.white,
          ),
          body: SingleChildScrollView(
            child: Container(
              //color: Colors.yellow,
              padding: EdgeInsets.only(top: 20, left: 10, right: 10),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "ENTER YOUR NEW PASSWORD",
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  TextFormField(
                    onChanged: (val){password=val;},
                    validator: (val){val.isEmpty?"Enter Password!":null;},
                    obscureText: true,
                    keyboardType: TextInputType.text,

                    decoration: InputDecoration(
                      counterText: "",
                      hintText: "Password",
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "CONFIRM YOUR NEW PASSWORD",
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  TextFormField(
                    //controller: passwordController,
                    onChanged: (val){confirmPassword=val;},
                    validator: (val){val.isEmpty?"Enter Password!":null;},
                    obscureText: _obscuredText,
                    keyboardType: TextInputType.text,

                    decoration: InputDecoration(
                      counterText: "",
                      hintText: "Password",
                      suffixIcon: IconButton(
                        color: Colors.lightGreen,
                        icon: Icon(
                            _obscuredText ? Icons.visibility : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            _obscuredText = !_obscuredText;
                          });
                        },
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: RaisedButton(
                      color: Colors.lightGreen,
                      child: Text(
                        "PROCEED",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: (){
//                          showDialog(
//                            context: context,
//                            barrierDismissible: false,
//                            builder:(BuildContext contextWait){
//                              return AlertDialog(
//                                content: Center(child: CircularProgressIndicator(backgroundColor: Colors.transparent,)),
//
//                              );
//                            }
//                          );
                        _ForgetPasswordUpdate(context);
                        //Navigator.pop(context);
                      },
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future _ForgetPasswordUpdate(BuildContext context)async{
    print("${password} ${confirmPassword} ${widget.userName}");
    print("Forget password Successfull");
    print(postForgetPassword);
    http.Response res =await http.post(postForgetPassword,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "username": "${widget.userName}",
        "password": "${password}",
        "newPassword": "${confirmPassword}"
      }),);
    print("REsponse : ${res.statusCode}");
    if (res.statusCode == 200){
      print("STATUS 200");
      print(res.body);
      Map<String,dynamic> forPassRes=jsonDecode(res.body) as Map<String,dynamic>;
      if(forPassRes['status']==true){
        //_scaffoldKey.currentState.showSnackBar(
        //  SnackBar(content: const Text("Password Updated Successfuly!"),)
        //);
        Navigator
            .of(context)
            .pushReplacement(new MaterialPageRoute(builder: (BuildContext context) => SignIn()));
      }else{
        print("FORGET PASS STATUS FALSE.......");
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return AlertDialog(
              title: Text("Error!"),
              content: Text("Password and Confirm Password not matched.Try Again!"),
              contentPadding: EdgeInsets.all(10),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
      }
    }else{
      print("FORGET PASS STATUS IS NOT 200.......");
    }
  }
}
