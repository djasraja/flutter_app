import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:flutter_shopping_app/DataBaseSqlite/DBHelper.dart';
import 'package:flutter_shopping_app/forgot_password.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:http/http.dart' as http;
import 'http_services.dart';
import 'package:flutter_shopping_app/Model/LoginModel.dart';
import 'package:flutter_shopping_app/Model/UserDetailsModel.dart';
import 'main.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}
final _formKey = GlobalKey<FormState>();
class _SignInState extends State<SignIn> {

  String mobileNo,password;
  final mobileNoController= TextEditingController();
  final passwordController= TextEditingController();
  bool _obscuredText = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    mobileNoController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(
          // resizeToAvoidBottomInset: false,
          // resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            title: Text(
              "SIGN IN",
              style: TextStyle(color: Colors.green),
            ),
            //backgroundColor: Colors.white,
          ),
          body: SingleChildScrollView(
            child: Container(
              //color: Colors.yellow,
              padding: EdgeInsets.only(top: 20, left: 10, right: 10),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "ENTER YOUR MOBILE NUMBER",
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  TextFormField(
                    controller: mobileNoController,
                    //onChanged: (val){mobileNo=val;},
                    validator: (val){val.isEmpty?"Enter Mobile Number!":null;},
                    maxLength: 10,
                    inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      counterText: "",
                      prefixText: "+91 ",
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "ENTER YOUR PASSWORD",
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  TextFormField(
                    controller: passwordController,
                    //onChanged: (val){password=val;},
                    validator: (val){val.isEmpty?"Enter Password!":null;},
                    obscureText: _obscuredText,
                    keyboardType: TextInputType.text,

                    decoration: InputDecoration(
                      counterText: "",
                      hintText: "Password",
                      suffixIcon: IconButton(
                        color: Colors.lightGreen,
                        icon: Icon(
                            _obscuredText ? Icons.visibility : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            _obscuredText = !_obscuredText;
                          });
                        },
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: RaisedButton(
                      color: Colors.lightGreen,
                      child: Text(
                        "SIGN IN",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: (){
//                          showDialog(
//                            context: context,
//                            barrierDismissible: false,
//                            builder:(BuildContext contextWait){
//                              return AlertDialog(
//                                content: Center(child: CircularProgressIndicator(backgroundColor: Colors.transparent,)),
//
//                              );
//                            }
//                          );
                         _login(context);
                        //Navigator.pop(context);
                      },
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  GestureDetector(
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "FORGOT PASSWORD",
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ),
                    onTap: () {
                      print( mobileNoController.text.isEmpty);
                      mobileNoController.text.isNotEmpty? Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => ForgotPassword(mobileNumber: int.tryParse(mobileNoController.text),),
                        ),
                      ):showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context){
                          return AlertDialog(
                            title: Text("Error!"),
                            content: Text("Please enter Mobile number to proceed!."),
                            contentPadding: EdgeInsets.all(10),
                            actions: <Widget>[
                              FlatButton(
                                child: Text("OK"),
                                onPressed: (){
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          );
                        },
                      );
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }



  Future _login(BuildContext context) async{
    //final form = _formKey.currentState;
    //if( _formKey.currentState.validate()){
      print('Login Successful');
      print('${mobileNoController.text} ${passwordController.text}');
      print(postLoginUrl);
      http.Response res =await http.post(postLoginUrl,
        headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
        "username": "${mobileNoController.text}",
        "password": "${passwordController.text}"
      }),);
        print("REsponse : ${res.statusCode}");
          if (res.statusCode == 200) {
            print("STATUS 200");
            print(res.body);
            LoginModel loginResBody=await loginModelFromJson(res.body);
            http.Response userVerifyViaTokenRes =await http.post(postUserVerifyViaTokenUrl,
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
              },
              body: jsonEncode(<String, String>{
                "token": "${loginResBody.token}",
              }),);
            print("user verify REsponse : ${userVerifyViaTokenRes.statusCode}");
            print(userVerifyViaTokenRes.body);
            if(userVerifyViaTokenRes.statusCode == 200){
              await DBHelper().addToLogin(loginResBody);
              if(loginResBody.status){
                await DBHelper().addToUserDetails(userDetailsModelFromJson(userVerifyViaTokenRes.body));
                print("Loggged In....");
                Navigator
                    .of(context)
                    .pushReplacement(new MaterialPageRoute(builder: (BuildContext context) => HomePage()));
              }else{
                await DBHelper().deleteFromLogin();
                print("ooppsss......SOMETHING WENT WRONG ! :(");
              }
            }
        }else{
          print("NOT Loggged In....");
        }
    //}
    //else{
    //  print('Login Failed!');
    //}
  }


}
