//import 'dart:html';

import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_shopping_app/DataBaseSqlite/DBHelper.dart';
import 'package:flutter_shopping_app/FireBaseService/fireBaseServices.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:http/http.dart' as http;
import 'Model/LoginModel.dart';
import 'Model/UserDetailsModel.dart';
import 'http_services.dart';
import 'main.dart';

Future<bool> verifyMobileNmber(String number)async{
  print("Number : ${number}");
  print("Replaced number : ${number.replaceFirst("+91", "")}");
  print(postCheckUserExistUrl);
  http.Response res =await http.post(postCheckUserExistUrl,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      "username": "${number.replaceFirst("+91", "")}"
    }),);
  if(res.statusCode == 200){
    print(res.body);
    print(jsonDecode(res.body)['status']);
    if(jsonDecode(res.body)['status']){
      return true;
    }else
      return false;
  }
}

class SignUP extends StatefulWidget {
  @override
  SignUPState createState() => SignUPState();
}

class SignUPState extends State<SignUP> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _obscuredText = true;
  String smsCode;
  String mobileNo,FName,Lname,Email,fire_UID,password,verificationId;

  static bool OTPVerificationflag=false;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(
          // resizeToAvoidBottomInset: false,
          // resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            title: Text(
              "Register",
              style: TextStyle(color: Colors.lightGreen),
            ),
            //backgroundColor: Colors.white,
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(top: 20, left: 10, right: 10),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Welcome to Zionx Ready!",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Enter Your Details to register.",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                  // Align(
                  //   alignment: Alignment.topLeft,
                  //   child: Text(
                  //     "AREA/Pin Code*",
                  //   ),
                  // ),
                  // Padding(
                  //   padding: EdgeInsets.all(5),
                  // ),
                  // TextFormField(
                  //   validator: (value){value.isEmpty?"Area/Pincode cannot be blank.":null;},
                  //   onChanged: (val) {this.pincode=val;},
                  //   maxLength: 6,
                  //   inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                  //   keyboardType: TextInputType.number,
                  //   decoration: InputDecoration(
                  //     counterText: "",
                  //     isDense: true,
                  //     contentPadding: EdgeInsets.all(8),
                  //     enabledBorder: OutlineInputBorder(
                  //       borderSide: BorderSide(color: Colors.black),
                  //     ),
                  //     focusedBorder: OutlineInputBorder(
                  //       borderSide: BorderSide(color: Colors.green),
                  //     ),
                  //   ),
                  // ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "FIRST NAME*",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    TextFormField(
                      validator: (value){value.isEmpty?"First Name cannot be blank.":null;},
                      onChanged: (val){this.FName=val;},
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        counterText: "",
                        isDense: true,
                        contentPadding: EdgeInsets.all(8),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "LAST NAME*",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    TextFormField(
                      validator: (value){value.isEmpty?"Last Name cannot be blank.":null;},
                      onChanged: (val){this.Lname=val;},
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        counterText: "",
                        isDense: true,
                        contentPadding: EdgeInsets.all(8),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "ENTER YOUR MOBILE NUMBER*",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    TextFormField(
                      validator: (value){value.isEmpty?"Mobile number cannot be blank.":null;},
                      onChanged: (val){this.mobileNo="+91"+val;},
                      maxLength: 10,
                      inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        counterText: "",
                        prefixText: "+91 ",
                        isDense: true,
                        contentPadding: EdgeInsets.all(8),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "EMAIL (Optional)",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    TextFormField(
                      onChanged: (val){this.Email=val;},
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        counterText: "",
                        isDense: true,
                        contentPadding: EdgeInsets.all(8),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "ENTER YOUR PASSWORD*",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    TextFormField(
                      validator: (value){value.isEmpty?"Password cannot be blank.":"BLah blah";},
                      onChanged: (val){
                          this.password=val;
                        },
                      obscureText: _obscuredText,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        counterText: "",
                        isDense: true,
                        contentPadding: EdgeInsets.all(8),
                        hintText: "Password",
                        suffixIcon: IconButton(
                          iconSize: 20,
                          color: Colors.lightGreen,
                          icon: Icon(_obscuredText
                              ? Icons.visibility
                              : Icons.visibility_off),
                          onPressed: () {
                            setState(() {
                              _obscuredText = !_obscuredText;
                            });
                          },
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    SizedBox(
                      height: 50,
                      width: double.infinity,
                      child: RaisedButton(
                        color: Colors.green,
                        child: Text(
                          "REGISTER",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onPressed:()async{
                          bool flag=await verifyMobileNmber(mobileNo);
                          if(flag){
                            showUserExistDialog(context,mobileNo);
                          }else
                           await _register();
                          } ,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _register() async{

    final form = _formKey.currentState;
    if (form.validate()) {
      print('Form is valid');
    } else {
      print('Form is invalid');
    }

    final PhoneVerificationCompleted verificationSuccessfull = (AuthCredential credential)async {
        Navigator.of(context).pop();
        print("Verification Successfull");
        print(credential);
      //await AuthServic().signIn(smsCode,verificationId);

    };

    final PhoneVerificationFailed verificationUnsuccessfull = (FirebaseAuthException exception){
        print("Verification Failed");
        print("${exception.message}");
    };

    final PhoneCodeSent smsSent = (String verId,[int forceResnd])async {
      this.verificationId = verId;
      await showOTPDialog(context).then((value){print("^^^^^^^^^^^^^^^^^^^^^^OTP DIALOG CLOSED");});
      print("Vriiiiiiiiiiiiiii   ----------------------  ${OTPVerificationflag}");
      if(OTPVerificationflag){
        this.fire_UID=FirebaseAuth.instance.currentUser.uid;
        print("^^^^^^^^^^^^^^^^^^^^^^${fire_UID}");
        await _registerUserWithDetails();
      }else{
        print("Something went wrong after OTP");
        await showWrongOTPDialog(context);
      }

    };

    final PhoneCodeAutoRetrievalTimeout autoTimeOut = (String verId){
      this.verificationId = verId;
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: this.mobileNo,
        timeout: Duration(seconds: 60),
        verificationCompleted: verificationSuccessfull,
        verificationFailed: verificationUnsuccessfull,
        codeSent: smsSent,
        codeAutoRetrievalTimeout: autoTimeOut
    );
  }

  Future showOTPDialog(BuildContext context) async{
    return showDialog(
        context: context,
      barrierDismissible: false,
      builder: (BuildContext context){
          return AlertDialog(
            title: Text("Enter OTP"),
            content: TextField(
              onChanged: (val){smsCode=val;},
            ),
            contentPadding: EdgeInsets.all(10),
            actions: <Widget>[
              FlatButton(
                child: Text("Verify"),
                onPressed: ()async {

                    await AuthServic().signIn(smsCode, verificationId);
                    if(FirebaseAuth.instance.currentUser != null){
                      print("user not null");
                      OTPVerificationflag= true;
                    }else{
                      print("user null");
                      OTPVerificationflag= false;
                    }
                    Navigator.of(context).pop();
                    //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomePage()));
                },
              )
            ],
          );
      },
    );
}


  Future showWrongOTPDialog(BuildContext context) async{
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text("INVALID OTP!"),
          content: Text("OTP Provided was WRONG! Try Again"),
          contentPadding: EdgeInsets.all(10),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: (){
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  Future<bool> showUserExistDialog(BuildContext context,String number) async{
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text("USER EXIST"),
          content: Text("Mobile number ${number.replaceFirst("+91", "")} is already registered!"),
          contentPadding: EdgeInsets.all(10),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: (){
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  Future _registerUserWithDetails()async{
    //String mobileNo,FName,Lname,Email,pincode,password,verificationId;
    print("${mobileNo} ${FName} ${Lname} ${Email} ${password} ${fire_UID}");
    print("Registration Successfull");
    print(postRegisterUserUrl);
    http.Response res =await http.post(postRegisterUserUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "firstName": "${FName}",
        "lastName": "${Lname}",
        "password": "${password}",
        "phone": "${mobileNo.replaceFirst("+91", "")}",
        "username": "${mobileNo.replaceFirst("+91", "")}",
        "fire_uid": "${fire_UID}",
        "emailId": "${Email}"
      }),);
    print("Register REsponse : ${res.statusCode}");
    if(res.statusCode==200){
      print("Registration STATUS 200");
      print(res.body);
      Map<String, dynamic> RegResList=jsonDecode(res.body) as Map<String,dynamic>;
      if(RegResList['status']==true){
        await DBHelper().deleteFromUserDetails();
        await DBHelper().deleteFromLogin();


        print(postLoginUrl);
        http.Response LoginRes =await http.post(postLoginUrl,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            "username": "${mobileNo.replaceFirst("+91", "")}",
            "password": "${password}"
          }),);
        print("REsponse of Reg login : ${LoginRes.statusCode}");
        if (LoginRes.statusCode == 200) {
          print("Reg login STATUS 200");
          print(LoginRes.body);
         //Map<String, dynamic> LoginResStatusVerficationBeforeLogin=jsonDecode(LoginRes.body) as Map<String,dynamic>;
         //if(LoginResStatusVerficationBeforeLogin['status']==true){
         //
         //}
          LoginModel loginResBody=await loginModelFromJson(LoginRes.body);
          http.Response userVerifyViaTokenRes =await http.post(postUserVerifyViaTokenUrl,
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, String>{
              "token": "${loginResBody.token}",
            }),);
          print("REGISTRATION user verify REsponse : ${userVerifyViaTokenRes.statusCode}");
          print(userVerifyViaTokenRes.body);
          if(userVerifyViaTokenRes.statusCode == 200){
            await DBHelper().addToLogin(loginResBody);
            if(loginResBody.status){
              await DBHelper().addToUserDetails(userDetailsModelFromJson(userVerifyViaTokenRes.body));
              print("Loggged In....AFTER REGISTRATION");
              Navigator
                  .of(context)
                  .pushReplacement(new MaterialPageRoute(builder: (BuildContext context) => HomePage()));
            }else{
              await DBHelper().deleteFromLogin();
              print("ooppsss......SOMETHING WENT WRONG ! :( AFTER REGISTRATION ");
            }
          }
        }else{
          print("NOT Loggged In....AFTER REGISTRATION");
        }



      }
    }
  }

}

