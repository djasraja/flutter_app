import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_shopping_app/ResetPassword.dart';
import 'package:flutter_shopping_app/theme.dart';

import 'FireBaseService/fireBaseServices.dart';
class ForgotPassword extends StatefulWidget {
  int mobileNumber;
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();

  ForgotPassword({@required this.mobileNumber});
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String convertedMobileNo='';
  String verificationId;
  String smsCode;
  static bool OTPVerificationflag=false;

  @override
  void initState() {
    // TODO: implement initState
    for(int i=0;i<widget.mobileNumber.toString().length;i++){
      if(i<6 && i>0)
        convertedMobileNo+='X';
      else
        convertedMobileNo+=widget.mobileNumber.toString()[i];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: basicTheme(),
      home: WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
        },
        child: Scaffold(

          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            title: Text(
              "FORGOT PASSWORD",
              style: TextStyle(color: Colors.lightGreen),
            ),
            //backgroundColor: Colors.white,
          ),
          body: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  //color: Colors.red,
                  height: MediaQuery.of(context).size.height*0.13,
                  child: Text("One Time Password (OTP) will be sent to your registered mobile number.",style: TextStyle(fontSize: 16,color: Colors.black87),),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  //color: Colors.red,
                  height: MediaQuery.of(context).size.height*0.05,
                  child: Text("Registered Mobile Number",style: TextStyle(fontSize: 16,color: Colors.black87),),
                ),
                Padding(
                  padding: EdgeInsets.all(2),
                ),
                TextFormField(
                  //controller: mobileNoController,
                  //onChanged: (val){mobileNo=val;},
                  initialValue: convertedMobileNo,
                  readOnly: true,
                  validator: (val){val.isEmpty?"Enter Mobile Number!":null;},
                  maxLength: 10,
                  inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    counterText: "",
                    prefixText: "+91 ",
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(3),
                ),
                SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: RaisedButton(
                    color: Colors.lightGreen,
                    child: Text(
                      "SEND OTP",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    onPressed: ()async{
                      await _ForgetPasswordOTPVerification();
                      //Navigator.pop(context);
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _ForgetPasswordOTPVerification() async{


    final PhoneVerificationCompleted verificationSuccessfull = (AuthCredential credential)async {
      Navigator.of(context).pop();
      print("Verification Successfull");
      print(credential);
      //await AuthServic().signIn(smsCode,verificationId);

    };

    final PhoneVerificationFailed verificationUnsuccessfull = (FirebaseAuthException exception){
      print("Verification Failed");
      print("${exception.message}");
    };

    final PhoneCodeSent smsSent = (String verId,[int forceResnd])async {
      this.verificationId = verId;
      await showOTPDialog(context).then((value){print("^^^^^^^^^^^^^^^^^^^^^^OTP DIALOG CLOSED");});
      print("Forget PAsssword   ----------------------  ${OTPVerificationflag}");
      if(!OTPVerificationflag){
        print("Something went wrong after OTP");
        await showWrongOTPDialog(context);
      }else{
        // _scaffoldKey.currentState..showSnackBar(
        //     SnackBar(
        //       content: const Text("Profile Successfully Updated!"),
        //     )
        // );
        print("OTP Verified Successfullt!");
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => ResetPassword(userName: widget.mobileNumber.toString(),)));
      }

    };

    final PhoneCodeAutoRetrievalTimeout autoTimeOut = (String verId){
      this.verificationId = verId;
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: "+91"+widget.mobileNumber.toString(),
        timeout: Duration(seconds: 60),
        verificationCompleted: verificationSuccessfull,
        verificationFailed: verificationUnsuccessfull,
        codeSent: smsSent,
        codeAutoRetrievalTimeout: autoTimeOut
    );
  }

  Future showOTPDialog(BuildContext context) async{
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text("Enter OTP"),
          content: TextField(
            onChanged: (val){smsCode=val;},
          ),
          contentPadding: EdgeInsets.all(10),
          actions: <Widget>[
            FlatButton(
              child: Text("Verify"),
              onPressed: ()async {

                await AuthServic().signIn(smsCode, verificationId);
                if(FirebaseAuth.instance.currentUser != null){
                print("user not null");
                print("Forget password successfull");
                OTPVerificationflag= true;
                }else{
                print("user null");
                OTPVerificationflag= false;
                }
                Navigator.of(context).pop();
                //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomePage()));
              },
            )
          ],
        );
      },
    );
  }

  Future showWrongOTPDialog(BuildContext context) async{
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text("INVALID OTP!"),
          content: Text("OTP Provided was WRONG! Try Again"),
          contentPadding: EdgeInsets.all(10),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: (){
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }


}
