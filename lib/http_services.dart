import 'dart:convert';

import 'package:http/http.dart';

const String getProductsUrl = "http://mrjay.in:3000/api/products/allProdsWithMulPrice";
const String postProductPriceTagsUrl="http://mrjay.in:3000/api/products/prices/byProdId";
const String postProductDetailsByPriceID="http://mrjay.in:3000/api/products/getProdByPriceId";
const String getPopularProductsUrl = "http://mrjay.in:3000/api/products/allProdsWithMulPrice";
const String getAddressDetailsUrl = "http://mrjay.in:3000/api/user/address/";
const String postDeleteAddressUrl="http://mrjay.in:3000/api/user/address/delete";
const String postEditAddressUrl="http://mrjay.in:3000/api/user/address/update";
const String postAddNewAddressUrl="http://mrjay.in:3000/api/user/address/new";
const String getProductByIdUrl = "http://10.0.2.2:3000/api/products/";
const String postLoginUrl="http://mrjay.in:3000/api/user/checkPassport";
const String postUserVerifyViaTokenUrl="http://mrjay.in:3000/api/user/userVerify";
const String postCheckUserExistUrl="http://mrjay.in:3000/api/user/checkUserExist";
const String postChangePasswordUrl="http://mrjay.in:3000/api/user/changePass";
const String postUpdateProfileUrl="http://mrjay.in:3000/api/user/profileUpdate";
const String postRefreshTokenUrl="http://mrjay.in:3000/api/user/refreshToken";
const String postRegisterUserUrl="http://mrjay.in:3000/api/user/new";
const String postForgetPassword="http://mrjay.in:3000/api/user/forgotPassword";
const String postOrderUrl="http://mrjay.in:3000/api/orders/new";

class HttpService {
  List datagot;
  Future getProducts() async {
    Map<String, dynamic> dataItems;
    print("Json Dat");
    Response res = await get(getProductsUrl);
    print("---------------------------${res.statusCode}");
    if (res.statusCode == 200) {
      dataItems = jsonDecode(res.body);

      datagot = dataItems['products'];
    } else {
      print("Json Data is NOT");
    }
    // print(datagot[0]['id']);
    return datagot.toList();
  }

//  Future getPopularProducts() async {
//    Map<String, dynamic> dataItems;
//    print("Json Datas");
//    Response res = await get(getPopularProductsUrl);
//    print("---------------------------${res.statusCode}");
//    if (res.statusCode == 200) {
//      print("---------------------------${res.statusCode}");
//      dataItems = jsonDecode(res.body);
//      datagot = dataItems['products'];
//    } else {
//      print("Json Data is NOT");
//    }
//    //print(datagot[0]['id']);
//    return datagot.toList();
//  }
}
