import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopping_app/ActionsPage.dart';
import 'package:flutter_shopping_app/AllCategoriesPage.dart';
import 'package:flutter_shopping_app/Model/UserDetailsModel.dart';
import 'package:flutter_shopping_app/SignIn.dart';
import 'package:flutter_shopping_app/SignUp.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:flutter_shopping_app/popUpActions.dart';
import 'package:flutter_shopping_app/theme.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:custom_navigator/custom_navigator.dart';

import 'BottomNavigationTab.dart';
import 'DataBaseSqlite/DBHelper.dart';
import 'FireBaseService/fireBaseServices.dart';
import 'ItemListViewGetter.dart';
import 'Model/CartModel.dart';
import 'Model/MyListModel.dart';
import 'ProductPage.dart';
import 'http_services.dart';


class popUpItems {
  popUpItems({this.values, this.icon});
  String values;
  IconData icon;
}

List<popUpItems> choicesBeforeLogin = <popUpItems>[
  popUpItems(values: "REGISTER", icon: MdiIcons.accountCircleOutline),
  popUpItems(values: "SIGN IN", icon: MdiIcons.login),
];

void main() {
    runApp(new MaterialApp(
      home: HomePage(),
    ));
    //runApp(HomePage());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: AuthServic().handleAuth(),
    );
  }
}


class NavigationDrawer extends StatefulWidget {
  @override
  _NavigationDrawerState createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  Future _getUserDetails;
  String firstName='Guest';
  Future<String> getFName()async{
    List<UserDetailsModel> userInfo=await DBHelper().getUserDetails();
    print(userInfo);
    print(userInfo.length);
    for(int i=0;i<userInfo.length;i++){
      print(userInfo[i].firstName);
      setState(() {
        firstName=userInfo[i].firstName;
      });
    }
    return 'Data Loaded';
  }

  @override
  void initState() {
    // TODO: implement initState
    setState(() {
      _getUserDetails=getFName();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
      return SizedBox(
            width: MediaQuery.of(context).size.width * 0.8,
            child: Drawer(
              child: FutureBuilder(
                future: _getUserDetails,
                builder: (BuildContext context, snapshot) {
                  print(snapshot.hasData);
                  print(snapshot.data);
                  if (snapshot.connectionState == ConnectionState.waiting)
                    return Center(child: CircularProgressIndicator());
                  else if(snapshot.hasData){
                    return Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          SizedBox(
                            height: MediaQuery.of(context).size.height*0.02,
                          ),
                          Row(
                            children: <Widget>[
                              IconButton(
                                icon: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.grey,
                                  size: 20,
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "Hi, ${firstName}",
                                style: TextStyle(
                                  fontSize: MediaQuery.of(context).size.width * 0.07,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.green,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              IconButton(
                                icon: Icon(
                                  MdiIcons.mapMarkerOutline,
                                ),
                                onPressed: () {},
                              ),
                              Text("380002",
                                  style: TextStyle(
                                    fontSize: MediaQuery.of(context).size.width * 0.04,
                                  )),
                              IconButton(
                                icon: Icon(
                                  MdiIcons.pencilOutline,
                                  size: 20,
                                ),
                                onPressed: () {},
                              ),
                            ],
                          ),
                          ButtonTheme(
                            height: 50,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => AllCategoriesPage(BottomBar: true,),
                                  ),
                                );
                              },
                              elevation: 15,
                              color: Colors.green,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    MdiIcons.viewGridOutline,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Text(
                                    "SHOP BY CATEGORY",
                                    style: TextStyle(color: Colors.white, fontSize: 18),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                      color: Colors.grey,
                                    ))),
                            child: ListTile(
                              leading: Icon(
                                MdiIcons.storeOutline,
                                color: Colors.green,
                                size: 30,
                              ),
                              title: Text(
                                "Pick Up Point List",
                                style: TextStyle(fontSize: 16),
                              ),
                              onTap: () {},
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                      color: Colors.grey,
                                    ))),
                            child: ListTile(
                              leading: Icon(
                                MdiIcons.clipboardTextOutline,
                                color: Colors.green,
                                size: 30,
                              ),
                              title: Text(
                                "Refund, Terms and Policies",
                                style: TextStyle(fontSize: 16),
                              ),
                              onTap: () {},
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                      color: Colors.grey,
                                    ))),
                            child: ListTile(
                              leading: Icon(
                                MdiIcons.commentQuestionOutline,
                                color: Colors.green,
                                size: 30,
                              ),
                              title: Text(
                                "Frequently Asked Questions",
                                style: TextStyle(fontSize: 16),
                              ),
                              onTap: () {},
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                }
              ),
            ),
          );
  }
}

class CarouselSliderGetter extends StatelessWidget {
  var CarouselSliderImages = [
    "assets/1.jpeg",
    "assets/2.jpeg",
    "assets/3.jpeg",
    "assets/4.jpeg",
    "assets/5.jpeg",
    "assets/6.jpeg"
  ];
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    print("----------------------------------CAROUSEL-------------------------------+++++");
    return CarouselSlider(
      options: CarouselOptions(
        height: 200,
        autoPlay: true,
        autoPlayCurve: Curves.fastOutSlowIn,
        aspectRatio: 16 / 9,
        enableInfiniteScroll: true,
        autoPlayAnimationDuration: Duration(milliseconds: 1500),
        viewportFraction: 0.8,
      ),
      items: [
        for (var items in CarouselSliderImages)
          Container(
            margin: EdgeInsets.all(5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                image: DecorationImage(
                    image: AssetImage(items), fit: BoxFit.fill)),
          ),
      ],
    );
  }
}

class ContainerWithTwoCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          GestureDetector(
              onTap: () {
                print("Be Healthy & Safe Category 1 Clicked");
              },
              child: Image.asset(
                "assets/4.jpeg",
                height: 110,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.fill,
              )),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  print("Card 1 Pressed");
                },
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Image.asset(
                    "assets/10.png",
                    fit: BoxFit.fill,
                    height: 200,
                    width: MediaQuery.of(context).size.width * 0.40,
                  ),
                  elevation: 10,
                ),
              ),
//              SizedBox(width: 10,),
              GestureDetector(
                onTap: () {
                  print("Card 2 Pressed");
                },
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Image.asset(
                    "assets/9.png",
                    fit: BoxFit.fill,
                    height: 200,
                    width: MediaQuery.of(context).size.width * 0.40,
                  ),
                  elevation: 10,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          GestureDetector(
              onTap: () {
                print("Be Healthy & Safe Category 2 Clicked");
              },
              child: Image.asset(
                "assets/1.jpeg",
                height: 110,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.fill,
              )),
        ],
      ),
    );
  }
}



class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //bool BottomBar = true;
  List popularProductItems;

  List<CartModel> _cartItem = [];

  int _getItemIndexFromCart(int newPriceID) {
    int index = -1;
    for (int i = 0; i < _cartItem.length; i++) {
      if (_cartItem[i].priceTagID == newPriceID) {
        index = i;
        break;
      }
    }
    return index;
  }

  Future getPopularProducts() async {
    List dataItems=[];
    print("Json Data home");
    http.Response res = await http.post(getPopularProductsUrl,
      headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "username": "nothing",
      }),
    );
    print("---------------------------${res.statusCode}");
    if(res.statusCode == 200){
      dataItems=jsonDecode(res.body);
      for(int i=0;i<dataItems.length;i++){
        http.Response productByProductIDResponse = await http.post(postProductPriceTagsUrl,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(<String, String>{
            "productId": "${dataItems[i]['id']}",
          }),
        );
        if(productByProductIDResponse.statusCode==200){
          List PriceTag=jsonDecode(productByProductIDResponse.body);
          dataItems[i]['newPriceID']=PriceTag[0]['id'];
          dataItems[i]['newMRP']=PriceTag[0]['mrp'];
          dataItems[i]['newPrice']=PriceTag[0]['price'];
          dataItems[i]['newStock']=PriceTag[0]['stock'];
          dataItems[i]['newUnitTitleID']=PriceTag[0]['unit'];
          dataItems[i]['newUnitQty']=PriceTag[0]['qty'];
          dataItems[i]['newUnitTitle']=PriceTag[0]['title'];
          dataItems[i]['newImg1']=PriceTag[0]['img'];
          dataItems[i]['priceTag']=PriceTag;
        }
      }
      //setState(() {
        popularProductItems=dataItems;
      //});
      print("${popularProductItems.length}");
      print("${popularProductItems[0]['priceTag'].runtimeType}");
      popularProductItems[0]['priceTag'].forEach((val){print("^^^^^^${val}");});
      print("${popularProductItems[0]['priceTag'].runtimeType}");
      print("${popularProductItems}");
    }else{
      print("Json Data is NOT");
    }
  }


  var PopularProductsitemDetails = [
    "assets/1.jpeg",
    "assets/2.jpeg",
    "assets/3.jpeg",
    "assets/4.jpeg",
    "assets/5.jpeg",
    "assets/6.jpeg"
  ];
  List<dynamic> CategoryItemDetails = [
    ["assets/1.jpeg", "Grocery"],
    ["assets/2.jpeg", "Dairy"],
    ["assets/3.jpeg", "Packaged Food"],
    ["assets/4.jpeg", "Fruits"],
    ["assets/5.jpeg", "Appliances"]
  ];
  int _currentIndex = 0;
  void _select(popUpItems item) {
    print(item.values);
    if (item.values == 'REGISTER') {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => SignUP(),
        ),
      );
    } else if (item.values == 'SIGN IN') {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => SignIn(),
        ),
      );
    }
  }


  void initState() {
    // TODO: implement initState
    //getPopularProducts();
    print("INIIIIIIIIIIIIIIIIIIIIIITTTTTTTTTTTTTTTTTTTtt");
    AuthServic().handleAuth();
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return new MaterialApp(
          theme: basicTheme(),
          title: "Shopping Application",
          home: Scaffold(
            drawer: NavigationDrawer(),
            appBar: AppBar(
              //backgroundColor: Colors.white,
              title: Text(
                "Zionx",
                style: TextStyle(color: Colors.lightGreen),
              ),
//          actions: <Widget>[
//            Container(
//              child: PopupMenuButton<popUpItems>(
//                onSelected: _select,
//                tooltip: "Actions",
//                itemBuilder: (BuildContext context) {
//                  return choicesBeforeLogin.map((popUpItems choice) {
//                    return PopupMenuItem<popUpItems>(
//                      value: choice,
//                      child: Container(
//                        //padding: EdgeInsets.all(4),
//                        child: Row(
//                          children: <Widget>[
//                            Icon(choice.icon),
//                            SizedBox(
//                              width: 10,
//                            ),
//                            Text(choice.values)
//                          ],
//                        ),
//                      ),
//                    );
//                  }).toList();
//                },
//              ),
//            )
//          ],
            ),
            bottomNavigationBar: BottomNavigationTab(
              currentIndex: 0,
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    //Horizontal Scrollable CAROUSEL
                    Container(
                      height: 200,
                      //color: Colors.green,
                      child: ListView(
                        children: <Widget>[
                          new CarouselSliderGetter(),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),

                    //SEASON OFFERS
                    Container(
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Seasonal Offers",
                              style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          GestureDetector(
                              onTap: () {
                                print("Seasonal Offers Clicked");
                              },
                              child: Image.asset(
                                "assets/1.jpeg",
                                fit: BoxFit.fill,
                                width: screenWidth,
                                height: 150,
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),

                    //Container with 2 cards
                    Container(
                      //color: Colors.greenAccent,
                      width: screenWidth,
                      height: 500,
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Be Healthy & Safe",
                              style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          ContainerWithTwoCards(),
                        ],
                      ),
                    ),

                    //Most Popular Products
                    Container(
                      //color: Colors.black26,
                      width: screenWidth,
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Most Popular Products",
                              style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          FutureBuilder(
                              future: Future.wait([DBHelper().getCartItems(),getPopularProducts()]),
                              builder: (context, snapShot){
                                if(snapShot.connectionState == ConnectionState.waiting){
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                }else{
                                  if (snapShot.hasData) _cartItem = snapShot.data[0];
                                  return ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: popularProductItems == null? 0:5,
                                    itemBuilder: (context, index) {
                                      return GestureDetector(
                                        onTap: () {
                                          print("$index Container pressed");
                                          setState(() {
                                            //BottomBar = false;
                                          });
                                          Navigator.push(context, MaterialPageRoute(builder: (_) => ProductPage(data: popularProductItems[index],),),);
                                          //Navigator.of(context, rootNavigator: true).pushReplacement(MaterialPageRoute(builder: (context) => ProductPage(data: popularProductItems[index],),));
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    width: 2, color: Colors.green)),
                                          ),
                                          //decoration: index % 2 == 0 ? BoxDecoration(color: Colors.orange) :BoxDecoration(color: Colors.deepOrangeAccent) ,
                                          height: 160,
                                          margin: EdgeInsets.all(5),
                                          child: Row(
                                            children: <Widget>[
                                              SizedBox(width: 10,),
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Image.network(popularProductItems[index]['newImg1'],fit: BoxFit.fill,height: 90,width: MediaQuery.of(context).size.width * 0.20,),
                                                  SizedBox(height: 15,),
                                                  GestureDetector(onTap: (){print("Added to list");addToList(popularProductItems[index]['id'],context);},child: Text("ADD TO LIST",style: TextStyle(fontSize: 14,color: Colors.green,fontWeight: FontWeight.bold),),),
                                                ],
                                              ),
                                              SizedBox(width: 10,),
                                              Container(
                                                width:
                                                MediaQuery.of(context).size.width *
                                                    0.67,
                                                //color: Colors.red,
                                                child: Column(
                                                  children: <Widget>[
                                                    Align(
                                                      alignment: Alignment.topLeft,
                                                      child: Container(
                                                        padding: EdgeInsets.all(5),
                                                        //color: Colors.blue,
                                                        height: 45,
                                                        child: Text(
                                                          "${popularProductItems[index]['title']} : ${popularProductItems[index]['newUnitQty']} ${popularProductItems[index]['newUnitTitle']}",
                                                          style: TextStyle(
                                                            fontSize: 18,
                                                            fontWeight: FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Align(
                                                      alignment: Alignment.centerRight,
                                                      child: Container(
                                                        //color: Colors.green,
                                                        height: 60,
                                                        child: Column(
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment.end,
                                                          children: <Widget>[
                                                            Text(
                                                              "MRP : \u{20B9}${popularProductItems[index]['newMRP']}",
                                                              style: TextStyle(
                                                                color: Colors.black87,
                                                                decoration:
                                                                TextDecoration
                                                                    .lineThrough,
                                                              ),
                                                            ),
                                                            Text(
                                                              "ZPrice : \u{20B9}${popularProductItems[index]['newPrice']}",
                                                              style: TextStyle(
                                                                  color: Colors.green,
                                                                  fontSize: 18),
                                                            ),
                                                            Text(
                                                              "Save : \u{20B9}${int.tryParse(popularProductItems[index]['newMRP']) - int.tryParse(popularProductItems[index]['newPrice'])}",
                                                              style: TextStyle(
                                                                  color: Colors.red,
                                                                  fontSize: 15),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Align(
                                                      alignment: Alignment.center,
                                                      child: Container(
                                                        //padding: EdgeInsets.all(5),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment.center,
                                                          crossAxisAlignment:
                                                          CrossAxisAlignment.center,
                                                          children: <Widget>[
                                                            Container(
                                                              //padding:
                                                              //EdgeInsets.all(8),
                                                              height: 35,
                                                              width: 100,
                                                              decoration: BoxDecoration(
                                                                  border: Border.all(
                                                                      width: 1),
                                                                  borderRadius:
                                                                  BorderRadius.all(
                                                                      Radius
                                                                          .circular(
                                                                          5))),
                                                              child:
                                                              new DropdownButton<dynamic>(
                                                                items: popularProductItems[index]['priceTag'].map<DropdownMenuItem<dynamic>>((mulPriceItem){
                                                                  return DropdownMenuItem(
                                                                    value: mulPriceItem,
                                                                    child: Text("${mulPriceItem['qty']} ${mulPriceItem['title']}"),
                                                                    onTap: (){},
                                                                  );
                                                                }).toList(),
                                                                onChanged: (mulPriceItem){
                                                                  //setState(() {
                                                                    print("Clicked ${mulPriceItem}");
                                                                    popularProductItems[index]['newPriceID']=mulPriceItem['id'];
                                                                    popularProductItems[index]['newMRP']=mulPriceItem['mrp'];
                                                                    popularProductItems[index]['newPrice']=mulPriceItem['price'];
                                                                    popularProductItems[index]['newStock']=mulPriceItem['stock'];
                                                                    popularProductItems[index]['newUnitTitleID']=mulPriceItem['unit'];
                                                                    popularProductItems[index]['newUnitQty']=mulPriceItem['qty'];
                                                                    popularProductItems[index]['newUnitTitle']=mulPriceItem['title'];
                                                                    popularProductItems[index]['newImg1']=mulPriceItem['img'];
                                                                  //});

                                                                },
                                                              ),
                                                              //Text(
                                                              //  "${popularProductItems[index]['newUnitQty']} ${popularProductItems[index]['newUnitTitle']}",
                                                              //  textAlign:
                                                              //  TextAlign.center,
                                                              //),
                                                            ),
                                                            SizedBox(width: 20),
                                                            (_getItemIndexFromCart(popularProductItems[index]['newPriceID']) < 0) ? ((popularProductItems[index]['newStock']>0)?RaisedButton(
                                                              onPressed: () async {
                                                                print("Add to cart");
                                                                await addToCart(popularProductItems[index]['id'],popularProductItems[index]['newPriceID'], context);
                                                                setState(() {});
                                                              },
                                                              color: Colors.green,
                                                              textColor: Colors.white,
                                                              child:
                                                              Text("ADD TO CART"),
                                                            ):RaisedButton(onPressed: (){},color: Colors.redAccent, child: Text("OUT OF STOCK", style: TextStyle(color: Colors.white),),)
                                                            ) : Container(
                                                              //padding: EdgeInsets.all(5),
                                                              child: Row(
                                                                mainAxisAlignment: MainAxisAlignment.end,
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: <Widget>[
                                                                  Container(
                                                                    alignment: Alignment.center,
                                                                    width: 40,
                                                                    height: 30,
                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60),topLeft: Radius.circular(60),)),
                                                                    child: RaisedButton(
                                                                      onPressed: () async{
                                                                        print("Minus");
                                                                        int i=_getItemIndexFromCart(popularProductItems[index]['newPriceID']);
                                                                        CartModel model=CartModel();
                                                                        var dbHelper=DBHelper();
                                                                        model.id=_cartItem[i].id;
                                                                        model.cartId=_cartItem[i].cartId;
                                                                        model.priceTagID=_cartItem[i].priceTagID;
                                                                        if(_cartItem[i].qty == 1){
                                                                          await dbHelper.deleteFromCart(model);
                                                                        }else{
                                                                          model.qty = _cartItem[i].qty - 1;
                                                                          await dbHelper.updateCart(model);
                                                                        }
                                                                        setState(() {});
                                                                      },
                                                                      color: Colors.white,
                                                                      textColor: Colors.green,
                                                                      child: Center(child: Text("-",style: TextStyle(fontSize: 20),)),
                                                                    ),
                                                                  ),
                                                                  SizedBox(width: 5,),
                                                                  Container(child: Text("${_cartItem[_getItemIndexFromCart(popularProductItems[index]['newPriceID'])].qty}",style: TextStyle(fontSize: 20),)),
                                                                  SizedBox(width: 5,),
                                                                  Container(
                                                                    alignment: Alignment.center,
                                                                    width: 40,
                                                                    height: 30,
                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomRight: Radius.circular(60),topRight: Radius.circular(60),)),
                                                                    child: RaisedButton(
                                                                      onPressed: () async {
                                                                        print("Plus");
                                                                        int i=_getItemIndexFromCart(popularProductItems[index]['newPriceID']);
                                                                        if((_cartItem[i].qty + 1) <= popularProductItems[index]['newStock']){
                                                                          CartModel model=CartModel();
                                                                          var dbHelper=DBHelper();
                                                                          model.id=_cartItem[i].id;
                                                                          model.cartId=_cartItem[i].cartId;
                                                                          model.priceTagID=_cartItem[i].priceTagID;
                                                                          model.qty = _cartItem[i].qty + 1;
                                                                          await dbHelper.updateCart(model);
                                                                        }else{
                                                                          Scaffold.of(context).showSnackBar(
                                                                            SnackBar(
                                                                              content: const Text('Reached maximum limit of product'),
                                                                            ),
                                                                          );
                                                                        }
                                                                        setState(() {});
                                                                      },
                                                                      color: Colors.white,
                                                                      textColor: Colors.green,
                                                                      child: Center(child: Text("+",style: TextStyle(fontSize: 20),)),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                }

                              }
                          ),

                          GestureDetector(
                            onTap: ()  {
                              print("View All Products");
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => ItemListViewGetter(itemsList: popularProductItems,),
                                ),
                              );
                            },
                            child: Container(
                              color: Colors.transparent,
                              width: screenWidth,
                              height: 45,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "View All",
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                  Icon(
                                    MdiIcons.forwardburger,
                                    color: Colors.green,
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    //Shop by Category
                    Container(
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Shop by Category",
                              style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          IgnorePointer(
                            child: GridView.count(
                              crossAxisSpacing: 1,
                              shrinkWrap: true,
                              crossAxisCount: 3,
                              children: <Widget>[
                                for (int i = 0;
                                    i < CategoryItemDetails.length;
                                    i++)
                                  Center(
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.width / 3,
                                      width:
                                          MediaQuery.of(context).size.width / 3,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Image.asset(CategoryItemDetails[i][0],
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  5,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  5),
                                          Text(CategoryItemDetails[i][1]),
                                        ],
                                      ),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 1, color: Colors.grey)),
                                    ),
                                  )
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              print("View All categories");
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => AllCategoriesPage(),
                                ),
                              );
                            },
                            child: Container(
                              color: Colors.transparent,
                              width: screenWidth,
                              height: 45,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "View All Categories",
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                                  Icon(
                                    MdiIcons.forwardburger,
                                    color: Colors.green,
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
  }

  void addToCart(int id,int newPriceID,BuildContext context) {
    CartModel list = new CartModel();
    list.cartId=id;
    list.priceTagID=newPriceID;
    list.qty=1;
    var dbHelper=DBHelper();
    dbHelper.addNewToCart(list);
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Added to Cart'),
//        action: SnackBarAction(
//            label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  void addToList(int id,BuildContext context) {
    MyListModel list = new MyListModel();
    list.listId=id;
    var dbHelper=DBHelper();
    dbHelper.addNewToList(list);
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Added to MY LIST'),
//        action: SnackBarAction(
//            label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
}
