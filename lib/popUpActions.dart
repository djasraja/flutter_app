
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';


class popUpItems{
  popUpItems({this.values,this.icon});
  String values;
  IconData icon;
}

//List<popUpItems> choices = <popUpItems>[
//  popUpItems(values:"REGISTER",icon: MdiIcons.accountCircleOutline),
//  popUpItems(values:"SIGN IN",icon: MdiIcons.login),
//];


class popUpButton extends StatefulWidget {
  final List<popUpItems> choices;


  popUpButton({this.choices});
  //final List<popUpItems> choices;
  @override
  State<StatefulWidget> createState() {
    return _popUpButtonState(
      choices: this.choices,
    );
  }
}

class _popUpButtonState extends State<popUpButton> {
  final List<popUpItems> choices;
  _popUpButtonState({this.choices});

  @override
  Widget build(BuildContext context) {
    void _select(popUpItems item){
//    setState(() {
//      selectedItem=item;
//    });
      print(item.values);
    }



    return PopupMenuButton<popUpItems>(
      onSelected: _select,
      tooltip: "Actions",
      itemBuilder: (BuildContext context) {
        return choices.map((popUpItems choice){
          return PopupMenuItem<popUpItems>(
            value: choice,
            child: Container(
              //padding: EdgeInsets.all(4),
              child: Row(
                children: <Widget>[
                  Icon(choice.icon),
                  SizedBox(width: 10,),
                  Text(choice.values)
                ],
              ),
            ),
          );
        }).toList();
      },
    );

  }
}
