

import 'package:flutter/material.dart';

ThemeData basicTheme(){
  TextTheme _basicTextTheme(TextTheme base){
    return base.copyWith(
      headline6: base.headline6.copyWith(
        fontWeight: FontWeight.bold,
        fontSize: 30.0,

      )
    );
  }
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    textTheme: _basicTextTheme(base.textTheme),
    primaryColor: Colors.white,
    appBarTheme: AppBarTheme(
      iconTheme: new IconThemeData(
        color: Colors.lightGreen,
      ),
    ),
  );
}